const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/components/datatables.js', 'public/js/components')
    .js('resources/js/components/input-file-upload.js', 'public/js/components')
    .js('resources/js/helpers/clubs.index.js', 'public/js/helpers')
    .js('resources/js/helpers/people.index.js', 'public/js/helpers')
    .js('resources/js/helpers/abilities.index.js', 'public/js/helpers')
    .js('resources/js/helpers/abilities.show.js', 'public/js/helpers')
    .sass('resources/scss/app.scss', 'public/css')
    .copy(
        'node_modules/@fortawesome/fontawesome-free/webfonts',
        'public/webfonts'
    );

if (mix.inProduction()) {
    mix.version();
} else {
    mix.browserSync({
        host: '127.0.0.1',
        proxy: 'localhost',
        open: false,
        watchOptions: {
                usePolling: true,
                interval: 500
      }
    });
}
