<?php

namespace Tests\Feature;

use App\Models\Person;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PersonScreensTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_view_a_list_of_people()
    {
        $user = User::factory()->create();
        $person = Person::factory()->create();

        $response = $this->actingAs($user)->get(
            route('people.index')
        );

        $response->assertOk();
        $response->assertSee($person->name);
    }

    public function test_a_user_can_view_person_details()
    {
        $user = User::factory()->create();
        $person = Person::factory()->create();

        $response = $this->actingAs($user)->get(
            route('people.show', ['person' => $person])
        );

        $response->assertOk();
        $response->assertSee($person->name);
    }

    public function test_a_user_can_edit_a_person()
    {
        $user = User::factory()->create();
        $person = Person::factory()->create();

        $response = $this->actingAs($user)->get(
            route('people.edit', ['person' => $person])
        );

        $response->assertOk();
        $response->assertSee($person->name);
    }

    public function test_a_user_can_save_an_edited_person()
    {
        $user = User::factory()->create();
        $person1 = Person::factory()->create();
        $person2 = Person::factory()->create();

        $response = $this->actingAs($user)->get(
            route('people.edit', ['person' => $person1])
        );

        $response = $this->actingAs($user)->put(
            route('people.update', ['person' => $person1]),
            $person2->toArray(),
        );

        $response->assertRedirect(route('people.show', ['person' => $person1]));
        $person1 = Person::find($person1->id);
        $person2 = Person::find($person2->id);

        // make people equal
        unset($person1['id'], $person1['created_at'], $person1['updated_at']);
        unset($person2['id'], $person2['created_at'], $person2['updated_at']);

        $this->assertEquals($person1->toArray(), $person2->toArray());
    }

    public function test_a_user_can_create_a_new_person()
    {
        Person::factory()->create();
        $person = Person::first();
        Person::where('id', '>', '0')->forceDelete();
        $person->id += 1;

        $new_person = $person->toArray();

        $user = User::factory()->create();
        $response = $this->actingAs($user)->get(
            route('people.create')
        );

        $response->assertOk();

        $response = $this->actingAs($user)->post(
            route('people.store'),
            $new_person,
        );

        $this->assertEquals(1, Person::count());

        $response->assertRedirect(route('people.show', ['person' => Person::first()]));

        $p1 = $person->toArray();
        $p2 = Person::first()->toArray();
        unset($p1['id'], $p1['created_at'], $p1['updated_at']);
        unset($p2['id'], $p2['created_at'], $p2['updated_at']);

        $this->assertEquals($p1, $p2);
    }

    public function test_a_user_can_trash_a_person()
    {
        $user = User::factory()->create();
        $person = Person::factory()->create();

        $response = $this->actingAs($user)->delete(
            route('people.destroy', ['person' => $person])
        );

        $response->assertRedirect(route('people.index'));
        $this->assertEquals(0, Person::count());
        $this->assertEquals(1, Person::withTrashed()->count());
    }

    public function test_a_user_cannot_see_deleted_person()
    {
        $user = User::factory()->create();
        $person = Person::factory()->create([
            'first_name' => substr(md5(rand()), 0, 10)
        ]);
        $person->delete();

        $response = $this->actingAs($user)->get(
            route('people.index')
        );

        $response->assertOk();
        $response->assertDontSee($person->firstName);
    }

    public function test_a_user_can_see_trashed_people()
    {
        $user = User::factory()->create();
        $person = Person::factory()->create([
            'first_name' => substr(md5(rand()), 0, 10)
        ]);
        $person->delete();

        $response = $this->actingAs($user)->get(
            route('people.index') . '?trashed'
        );

        $response->assertOk();
        $response->assertSee($person->name);
    }
}
