<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserScreensTest extends TestCase
{
    use RefreshDatabase;


    public function test_a_user_can_see_list_of_users()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(
            route('users.index')
        );

        $response->assertOk();
        $response->assertsee($user->username);
        $response->assertsee($user->name);
    }

    public function test_a_user_can_view_his_profile()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(
            route('users.show', ['user' => $user])
        );

        $response->assertOk();
        $response->assertSee($user->name);
        $response->assertSee($user->username);
        $response->assertSee($user->email);
        $response->assertDontSee($user->password);
    }

    public function test_a_user_can_view_other_profiles()
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();

        $response = $this->actingAs($user1)->get(
            route('users.show', ['user' => $user2])
        );

        $response->assertOk();
        $response->assertSee($user2->name);
        $response->assertSee($user2->username);
        $response->assertSee($user2->email);
    }

    public function test_a_user_cannot_edit_other_profiles()
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();

        $response = $this->actingAs($user1)->get(
            route('users.edit', ['user' => $user2])
        );

        $response->assertStatus(403);
    }

    public function test_a_user_cannot_view_non_existing_profiles()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/users/2');
        $response->assertStatus(404);
    }

    public function test_a_user_can_update_his_profile_without_new_password()
    {
        $user = User::factory()->create(['password' => Hash::make('password')]);
        $this->setup_user_edit($user);

        $new_user = User::factory()->make();

        $response = $this->actingAs($user)->put(
            route('users.update', ['user' => $user]),
            [
                'name' => $new_user->name,
                'username' => $new_user->username,
                'email' => $new_user->email,
            ]
        );

        // get the new user
        $user = $user->fresh();

        // Check if user has been updated
        $response->assertRedirect(route('users.show', ['user' => $user]));
        $user = User::first();
        $this->assertEquals($new_user->name, $user->name);
        $this->assertEquals($new_user->username, $user->username);
        $this->assertEquals($new_user->email, $user->email);
    }

    public function test_a_user_editing_must_enter_same_password_twice()
    {
        $user = User::factory()->create(['password' => Hash::make('password')]);
        $this->setup_user_edit($user);

        $new_password = Str::random(16);
        $new_user = User::factory()->make();

        $response = $this->actingAs($user)->put(
            route('users.update', ['user' => $user]),
            [
                'name' => $new_user->name,
                'username' => $new_user->username,
                'email' => $new_user->email,
                'password' => $new_password,
                'password_confirmation' => $new_password . 'invalid',
            ]
        );

        $response->assertSessionHasErrors('password');
    }

    public function test_a_user_can_change_his_password()
    {
        $user = User::factory()->create(['password' => Hash::make('password')]);
        $this->setup_user_edit($user);

        $new_password = Str::random(16);

        $response = $this->actingAs($user)->put(
            route('users.update', ['user' => $user]),
            [
                'name' => $user->name,
                'username' => $user->username,
                'email' => $user->email,
                'password' => $new_password,
                'password_confirmation' => $new_password,
            ]
        );

        $response->assertRedirect(route('users.show', ['user' => $user]));

        // Check if password has been updated
        $user = User::first();
        $this->assertTrue(Hash::check($new_password, $user->password));
    }

    private function setup_user_edit(User $user) {
        $response = $this->actingAs($user)->get(
            route('users.edit', ['user' => $user])
        );

        // System must ask for password
        $response->assertRedirect('confirm-password');
        $response = $this->actingAs($user)->post(
            'confirm-password',
            ['password' => 'password',]
        );

        // Check edit screen
        $response = $this->actingAs($user)->get(
            route('users.edit', ['user' => $user])
        );
        $response->assertSee('Αλλαγή κωδικού');
    }
}
