<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GuestTest extends TestCase
{
    public function test_guest_is_redirected_to_login()
    {
        $response = $this->get('/');

        $response->assertRedirect(route('login'));
    }
}
