<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\Club;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ClubScreensTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_view_a_list_of_clubs()
    {
        $user = User::factory()->create();
        $club = Club::factory()->create();

        $response = $this->actingAs($user)->get(
            route('clubs.index')
        );

        $response->assertOk();
        $response->assertSee($club->name);
    }

    public function test_a_user_can_view_club_details()
    {
        $user = User::factory()->create();
        $club = Club::factory()->create();

        $response = $this->actingAs($user)->get(
            route('clubs.show', ['club' => $club])
        );

        $response->assertOk();
        $response->assertSee($club->name);
    }

    public function test_a_user_can_edit_a_club()
    {
        $user = User::factory()->create();
        $club = Club::factory()->create();

        $response = $this->actingAs($user)->get(
            route('clubs.edit', ['club' => $club])
        );

        $response->assertOk();
        $response->assertSee($club->name);
    }

    public function test_a_user_can_save_an_edited_a_club()
    {
        $user = User::factory()->create();
        $club1 = Club::factory()->create();
        $club2 = Club::factory()->create();

        $response = $this->actingAs($user)->get(
            route('clubs.edit', ['club' => $club1])
        );

        $response = $this->actingAs($user)->put(
            route('clubs.update', ['club' => $club1]),
            array_merge($club2->toArray(), [
                'name' => $club1->name,    // unique field
                'email' => $club1->email,  // unique field
                'slug' => $club1->slug,  // unique field
            ]),
        );

        $response->assertRedirect(route('clubs.show', ['club' => $club1]));
        $club1 = Club::find($club1->id);
        $club2 = Club::find($club2->id);

        // make clubs equal
        $club1->id = $club2->id;
        $club1->name = $club2->name;
        $club1->email = $club2->email;
        $club1->slug = $club2->slug;
        $club1->updated_at = $club2->updated_at;

        $this->assertEquals($club1->toArray(), $club2->toArray());
    }

    public function test_a_user_can_create_a_new_club()
    {
        Club::factory()->create();
        $club = Club::first();
        Club::where('id', '>', '0')->forceDelete();
        $club->id += 1;

        $new_club = $club->toArray();
        $new_address = Address::factory()->make();

        $user = User::factory()->create();
        $response = $this->actingAs($user)->get(
            route('clubs.create')
        );

        $response->assertOk();

        $response = $this->actingAs($user)->post(
            route('clubs.store'),
            array_merge(
                $new_club,
                [
                    'address_main' => 'row-0',
                    'address_id' => ['new'],
                    'address_deleted' => ['false'],
                    'address_street' => [$new_address->street],
                    'address_region' => [$new_address->region],
                    'address_zip' => [$new_address->zip],
                    'address_city' => [$new_address->city],
                    'address_country' => [$new_address->country],
                    'address_phone' => [$new_address->phone],
                ]
            ),
        );

        $this->assertEquals(1, Club::count());
        $this->assertEquals(1, Club::first()->addresses()->count());

        $response->assertRedirect(route('clubs.show', ['club' => Club::first()]));

        $c1 = $club->toArray();
        $c2 = Club::first()->toArray();
        unset($c1['id'], $c1['created_at'], $c1['updated_at']);
        unset($c2['id'], $c2['created_at'], $c2['updated_at']);
        $this->assertEquals($c1, $c2);
    }

    public function test_a_user_can_trash_a_club()
    {
        $user = User::factory()->create();
        $club = Club::factory()->create();

        $response = $this->actingAs($user)->delete(
            route('clubs.destroy', ['club' => $club])
        );

        $response->assertRedirect(route('clubs.index'));
        $this->assertEquals(0, Club::count());
        $this->assertEquals(1, Club::withTrashed()->count());
    }

    public function test_a_user_cannot_see_deleted_club()
    {
        $user = User::factory()->create();
        $club = Club::factory()->create([
            'name' => substr(md5(rand()), 0, 10)
        ]);
        $club->delete();

        $response = $this->actingAs($user)->get(
            route('clubs.index')
        );

        $response->assertOk();
        $response->assertDontSee($club->name);
    }

    public function test_a_user_can_see_trashed_clubs()
    {
        $user = User::factory()->create();
        $club = Club::factory()->create([
            'name' => substr(md5(rand()), 0, 10)
        ]);
        $club->delete();

        $response = $this->actingAs($user)->get(
            route('clubs.index') . '?trashed'
        );

        $response->assertOk();
        $response->assertSee($club->name);
    }
}
