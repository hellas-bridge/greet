## G.R.E.E.T.

Το σύστημα <span style="color:cyan">G</span>REEK B<span style="color:cyan">R</span>IDG<span style="color:cyan">E</span> F<span style="color:cyan">E</span>DRA<span style="color:cyan">T</span>ION βασίζεται στο Laravel έκδοση 8.0 και χρειάζεται τις ακόλουθες τεχνολογίες:

- PHP 8.0
- MySQL 8.0
- Memcache
- Redis

## Εγκατάσταση

    clone
    cp .env.example .env
    composer install
    npm install
    npm run dev
    php artisan migrate
    php artisan storage:link
    php artisan serve

## Χρήση

- URL: http://localhost
- Mailhog: http://localhost:8025/
