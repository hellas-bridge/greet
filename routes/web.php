<?php

use App\Models\Person;
use Illuminate\Support\Facades\Route;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'cookies'])->group(function () {
    Route::get('/', function () {
        return view('dashboard');
    })->name('home');

    Route::resource('users', App\Http\Controllers\UserController::class);
    Route::resource('clubs', App\Http\Controllers\ClubController::class);
    Route::resource('people', App\Http\Controllers\PersonController::class);
    Route::resource('abilities', App\Http\Controllers\AbilityController::class);

});

require __DIR__.'/auth.php';
