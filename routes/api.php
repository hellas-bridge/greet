<?php

use App\Models\AbilityClubPerson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cookie;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/sidebar-toggle', function () {
    $sidebar = Cookie::get('sidebar') ?: '';
    if ($sidebar == '') {
        $sidebar = 'collapsed';
    } else {
        $sidebar = '';
    }
    return $sidebar;
});

Route::get('/people', [\App\Http\Controllers\API\PersonController::class, 'index']);

Route::get('/abilities', [\App\Http\Controllers\API\AbilityController::class, 'index']);
Route::get('/abilities/{ability}', [\App\Http\Controllers\API\AbilityController::class, 'show']);

Route::post('/ability-club-person', [\App\Http\Controllers\API\AbilityClubPersonController::class, 'store']);
Route::patch('/ability-club-person/{id}', [\App\Http\Controllers\API\AbilityClubPersonController::class, 'switch']);
Route::delete('/ability-club-person/{id}', [\App\Http\Controllers\API\AbilityClubPersonController::class, 'delete']);

Route::get('/search/people/{query}', [\App\Http\Controllers\API\SearchController::class, 'person']);
Route::get('/search/clubs/{query}', [\App\Http\Controllers\API\SearchController::class, 'club']);
