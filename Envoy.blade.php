@servers(['web' => 'pournaras@hellasbridge.org', 'local' => '127.0.0.1'])

@setup
    $file = file_get_contents('./public/new-features.md');
    $start = strpos($file, '#####') + 6;
    $end = strpos($file, '####', $start);
    $file = substr($file, $start, $end-$start);

@endsetup

@story('deploy', ['on' => 'web'])
    confirm-assets
    ask-question
    set-offline
    pull-from-bitbucket
    update-files
    prepare-composer
    migrate-database
    clear-optimization
    optimize
    set-online
@endstory

@task('confirm-assets', ['on' => 'local'])
    echo "Before starting (READ CAREFULLY):"
    echo "* edit new-features.md"
    echo "* npm run prod"
    echo "* git push"
    echo ""
    echo "(press ctrl+c to abort)"
@endtask

@task('ask-question', ['on' => 'local', 'confirm' => 'true'])
    echo ""
@endtask

@task('set-offline')
    echo "Setting the application offline"
    cd /home/pournaras/code/greet
    php artisan down
@endtask

@task('pull-from-bitbucket')
    echo "Pulling code from bitbucket"
    cd /home/pournaras/code/greet
    git checkout master
    git stash
    git stash drop
    git pull
@endtask

@task('update-files')
    echo "Preparing env variables"
    cd /home/pournaras/code/greet
    rm -f docker-compose.yml
    cp docker-compose-server.yml docker-compose.yml
    rm -f .env
    cp .env.prod .env
@endtask

@task('prepare-composer')
    echo "Composer setup"
    cd /home/pournaras/code/greet
    composer install --ignore-platform-reqs --no-interaction --no-progress --no-scripts --optimize-autoloader
@endtask

@task('migrate-database')
    echo "Migrating database"
    cd /home/pournaras/code/greet
    docker-compose exec -T -u sail laravel.test php artisan migrate --force
@endtask

@task('clear-optimization')
    echo "Clearing previous optimization"
    cd /home/pournaras/code/greet
    docker-compose exec -T -u sail laravel.test php artisan optimize:clear
@endtask

@task('optimize')
    echo "Optimizing files"
    cd /home/pournaras/code/greet
    docker-compose exec -T -u sail laravel.test php artisan optimize
@endtask

@task('set-online')
    echo "Setting the application online"
    cd /home/pournaras/code/greet
    php artisan up
@endtask

@finished
    @slack('https://hooks.slack.com/services/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', '#software-greet', "{$file}")
@endfinished

@error
    echo $task;
@enderror
