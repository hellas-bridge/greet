<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $country = $this->faker->optional()->country();
        if ($country == 'Ελλάδα') {
            $country = null;
        }
        return [
            'main' => $this->faker->boolean(),
            'phone' => $this->faker->optional()->numerify('##########'),
            'street' => $this->faker->optional()->streetAddress(),
            'region' => $this->faker->optional()->citySuffix(),
            'zip' => $this->faker->optional()->postcode(),
            'city' => $this->faker->optional()->city(),
            'country' => $country,
        ];
    }
}
