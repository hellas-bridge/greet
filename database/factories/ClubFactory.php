<?php

namespace Database\Factories;

use App\Models\Club;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ClubFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Club::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->unique(false, '50000')->word();
        return [
            'name' => Str::upper($name),
            'email' => $name . '@hellasbridge.org',
            'active' => $this->faker->boolean(50),
            'normal_member' => $this->faker->boolean(50),
            'in_athens' => $this->faker->boolean(50),
            'in_major_city' => $this->faker->boolean(50),
            'full_name' => $this->faker->company(),
            'founded' => $this->faker->date('d/m/Y', '-5 years'),
            'website' => $this->faker->optional()->url(),
            'irs_vat_number' => (string)$this->faker->optional()->numberBetween(111111111,909999999),
            'irs_location' => $this->faker->optional()->city(),
            'irs_headquarters' => $this->faker->optional()->city(),
            'bylaws' => $this->faker->optional()->words(3, true),
            'subscription_date' => $this->faker->date('d/m/Y'),
            'subscription_protocol' => $this->faker->optional()->words(3, true),
            'membership_date' => $this->faker->optional()->date('d/m/Y'),
            'membership_protocol' => $this->faker->optional()->words(3, true),
            'gga_code' => Str::upper($this->faker->optional()->word()),
            'gga_protocol' => $this->faker->optional()->words(3, true),
            'gga_recognized' => $this->faker->boolean(50),
        ];
    }
}
