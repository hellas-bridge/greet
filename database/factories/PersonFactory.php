<?php

namespace Database\Factories;

use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Person::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sex = rand(0, 1);
        $type = $sex == 1 ? 'male' : 'female';
        $firstName = $type == 'male' ? $this->faker->firstNameMale() : $this->faker->firstNameFemale();

        return [
            'last_name' => $this->faker->lastName($type),
            'first_name' => $firstName,
            'initial' => mb_substr($firstName, 0, 1),
            'email' => $this->faker->email(),
            'phone' => $this->faker->optional()->numerify('##########'),
            'mobile' => $this->faker->optional()->numerify('##########'),
            'male' => $sex,
            'birth_date' => $this->faker->date('d/m/Y', '-10 years'),
            'profession' => $this->faker->optional()->word(),
            'identity_card' => $this->faker->optional()->word(),
        ];
    }
}
