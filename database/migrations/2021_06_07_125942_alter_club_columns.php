<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterClubColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->renameColumn('vat_number', 'irs_vat_number');
            $table->renameColumn('vat_location', 'irs_location');
            $table->renameColumn('vat_headquarters', 'irs_headquarters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->renameColumn('irs_vat_number', 'vat_number');
            $table->renameColumn('irs_location', 'vat_location');
            $table->renameColumn('irs_headquarters', 'vat_headquarters');
        });
    }
}
