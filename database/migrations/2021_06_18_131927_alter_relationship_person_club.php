<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRelationshipPersonClub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->unsignedBigInteger('manager_id')->after('website')->nullable();
        });
        Schema::table('people', function (Blueprint $table) {
            $table->dropColumn('club_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('manager_id');
        });
        Schema::table('people', function (Blueprint $table) {
            $table->unsignedBigInteger('club_id')->after('parent_id')->nullable();
            $table->index('club_id');
        });
    }
}
