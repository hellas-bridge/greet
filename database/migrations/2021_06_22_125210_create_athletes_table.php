<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAthletesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('athletes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('person_id')->unique();
            $table->unsignedMediumInteger('code')->nullable()->unique();
            $table->date('subscription');
            $table->date('update');
            $table->unsignedBigInteger('club_id')->nullable();
            $table->unsignedTinyInteger('category')->default(1);
            $table->integer('black')->default(0);
            $table->integer('gold')->default(0);
            $table->decimal('platinum', 8, 1)->default(0);

            $table->boolean('print_card')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->index('person_id');
            $table->index('code');
            $table->index('club_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('athletes');
    }
}
