<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->boolean('active')->default(true);
            $table->boolean('normal_member')->default(false);
            $table->boolean('in_athens')->default(false);
            $table->boolean('in_major_city')->default(false);
            $table->string('full_name')->nullable();

            $table->date('founded')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('website')->nullable();

            $table->string('vat_number')->nullable();
            $table->string('vat_location')->nullable();
            $table->string('vat_headquarters')->nullable();

            $table->string('bylaws')->nullable();

            $table->date('subscription_date');
            $table->string('subscription_protocol')->nullable();
            $table->date('membership_date')->nullable();
            $table->string('membership_protocol')->nullable();

            $table->string('gga_code')->nullable();
            $table->string('gga_protocol')->nullable();
            $table->boolean('gga_recognized')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
