<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('last_name');
            $table->string('first_name')->nullable();
            $table->string('initial')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->boolean('male')->default(true);
            $table->date('birth_date')->nullable();
            $table->unsignedMediumInteger('birth_year')->virtualAs('YEAR(birth_date)')->nullable();
            $table->string('profession')->nullable();
            $table->string('identity_card')->nullable();
            // points to parent person, in case this person gets merged with another person
            // uses App\Traits\SelfReferenceTrain
            $table->unsignedBigInteger('parent_id')->nullable();
            // points to club the user runs
            $table->unsignedBigInteger('club_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index('last_name');
            $table->index('first_name');
            $table->index('club_id');
            $table->index('birth_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
