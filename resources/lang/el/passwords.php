<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Έγινε επαναφορά του κωδικού πρόσβασής σας!',
    'sent' => 'Σας στείλαμε στη διεύθυνσή σας έναν σύνδεσμο για επαναφορά του κωδικού σας.',
    'throttled' => 'Παρακαλούμε περιμένετε πριν δοκιμάσετε ξανά.',
    'token' => 'Ο σύνδεσμος επαναφοράς κωδικού πρόσβασης είναι άκυρος.',
    'user' => "Δε βρήκαμε χρήστη με αυτήν τη διεύθυνση.",

];
