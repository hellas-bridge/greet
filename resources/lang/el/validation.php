<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Το πεδίο :attribute πρέπει να γίνει αποδεκτό.',
    'active_url' => 'Το πεδίο :attribute δεν είναι δόκιμο URL.',
    'after' => 'Το πεδίο :attribute πρέπει να είναι ημερομηνία έπειτα από :date.',
    'after_or_equal' => 'Το πεδίο :attribute πρέπει να είναι ημερομηνία μεγαλύτερη ή ίση με :date.',
    'alpha' => 'Το πεδίο :attribute πρέπει να περιέχει μόνο γράμματα.',
    'alpha_dash' => 'Το πεδίο :attribute πρέπει να περιέχει μόνο γράμματα, αριθμούς, παύλες και κάτω παύλες.',
    'alpha_num' => 'Το πεδίο :attribute πρέπει να περιέχει μόνο γράμματα και αριθμούς.',
    'array' => 'Το πεδίο :attribute πρέπει να είναι μήτρα στοιχείων.',
    'before' => 'Το πεδίο :attribute πρέπει να είναι ημερομηνία πριν από :date.',
    'before_or_equal' => 'Το πεδίο :attribute πρέπει να είναι ημερομηνία μικρότερη ή ίση με :date.',
    'between' => [
        'numeric' => 'Το πεδίο :attribute πρέπει να είναι μεταξύ :min και :max.',
        'file' => 'Το αρχείο :attribute πρέπει να έχει μέγεθος μεταξύ :min και :max kilobytes.',
        'string' => 'Το πεδίο :attribute πρέπει να έχει μεταξύ :min και :max χαρακτήρες.',
        'array' => 'Το πεδίο :attribute πρέπει να έχει μεταξύ :min και :max αντικείμενα.',
    ],
    'boolean' => 'Το πεδίο :attribute πρέπει να είναι αληθές ή ψευδές.',
    'confirmed' => 'Το πεδίο επιβεβαίωσης :attribute δεν ταιριάζει.',
    'date' => 'Το πεδίο :attribute δεν είναι δόκιμη ημερομηνία.',
    'date_equals' => 'Το πεδίο :attribute πρέπει να είναι ημερομηνία ίση με :date.',
    'date_format' => 'Το πεδίο :attribute δεν έχει τη μορφή :format.',
    'different' => 'Το πεδίο :attribute και το :other πρέπει να είναι διαφορετικά.',
    'digits' => 'Το πεδίο :attribute πρέπει να έχει :digits ψηφία.',
    'digits_between' => 'Το πεδίο :attribute πρέπει να είναι μεταξύ :min και :max ψηφία.',
    'dimensions' => 'Το αρχείο :attribute δεν έχει έγκυρες διαστάσεις.',
    'distinct' => 'Το πεδίο :attribute έχει διπλότυπη τιμή.',
    'email' => 'Το πεδίο :attribute πρέπει να είναι έγκυρη διεύθυνση email.',
    'ends_with' => 'Το πεδίο :attribute πρέπει να καταλήγει με ένα από τα ακόλουθα: :values.',
    'exists' => 'Η επιλεγμένη τιμή του πεδίου :attribute είναι άκυρη.',
    'file' => 'Το πεδίο :attribute πρέπει να είναι αρχείο.',
    'filled' => 'Το πεδίο :attribute πρέπει να έχει τιμή.',
    'gt' => [
        'numeric' => 'Το πεδίο :attribute πρέπει να είναι μεγαλύτερο από :value.',
        'file' => 'Το αρχείο :attribute πρέπει να έχει μέγεθος μεγαλύτερο από :value kilobytes.',
        'string' => 'Το πεδίο :attribute πρέπει να έχει μέγεθος μεγαλύτερο από :value χαρακτήρες.',
        'array' => 'Το πεδίο :attribute πρέπει να έχει τουλάχιστον :value αντικείμενα.',
    ],
    'gte' => [
        'numeric' => 'Το πεδίο :attribute πρέπει να είναι μεγαλύτερο ή ίσο με :value.',
        'file' => 'Το αρχείο :attribute πρέπει να έχει μέγεθος μεγαλύτερο ή ίσο με :value kilobytes.',
        'string' => 'Το πεδίο :attribute πρέπει να έχει μέγεθος μεγαλύτερο ή ίσο με :value χαρακτήρες.',
        'array' => 'Το πεδίο :attribute πρέπει να έχει τουλάχιστον :value αντικείμενα.',
    ],
    'image' => 'Το πεδίο :attribute πρέπει να είναι εικόνα.',
    'in' => 'Το επιλεγμένο πεδίο :attribute είναι άκυρο.',
    'in_array' => 'Το πεδίο :attribute δεν ανήκει στα :other.',
    'integer' => 'Το πεδίο :attribute πρέπει να είναι ακέραιος.',
    'ip' => 'Το πεδίο :attribute πρέπει να είναι έγκυρη IP διεύθυνση.',
    'ipv4' => 'Το πεδίο :attribute πρέπει να είναι έγκυρη IPv4 διεύθυνση.',
    'ipv6' => 'Το πεδίο :attribute πρέπει να είναι έγκυρη IPv6 διεύθυνση.',
    'json' => 'Το πεδίο :attribute να είναι έγκυρη σειρά JSON.',
    'lt' => [
        'numeric' => 'Το πεδίο :attribute πρέπει να είναι μικρότερο από :value.',
        'file' => 'Το αρχείο :attribute πρέπει να έχει μέγεθος μικρότερο από :value kilobytes.',
        'string' => 'Το πεδίο :attribute πρέπει να έχει μέγεθος μικρότερο από :value χαρακτήρες.',
        'array' => 'Το πεδίο :attribute πρέπει να έχει λιγότερα από :value αντικείμενα.',
    ],
    'lte' => [
        'numeric' => 'Το πεδίο :attribute πρέπει να είναι μικρότερο ή ίσο με :value.',
        'file' => 'Το αρχείο :attribute πρέπει να έχει μέγεθος μικρότερο ή ίσο με :value kilobytes.',
        'string' => 'Το πεδίο :attribute πρέπει να έχει μέγεθος μικρότερο ή ίσο με :value χαρακτήρες.',
        'array' => 'Το πεδίο :attribute δεν πρέπει να έχει περισσότερα από :value αντικείμενα.',
    ],
    'max' => [
        'numeric' => 'Το πεδίο :attribute δεν μπορεί ναι είναι μεγαλύτερο από :max.',
        'file' => 'Το αρχείο :attribute δεν μπορεί να έχει μέγεθος μεγαλύτερο από :max kilobytes.',
        'string' => 'Το πεδίο :attribute δεν πρέπει να έχει περισσότερους από :max χαρακτήρες.',
        'array' => 'Το πεδίο :attribute δεν πρέπει να έχει περισσότερα από :max αντικείμενα.',
    ],
    'mimes' => 'Το αρχείο :attribute πρέπει να είναι τύπου: :values.',
    'mimetypes' => 'Το αρχείο :attribute πρέπει να είναι τύπου: :values.',
    'min' => [
        'numeric' => 'Το πεδίο :attribute πρέπει να είναι τουλάχιστον :min.',
        'file' => 'Το αρχείο :attribute πρέπει να έχει μέγεθος τουλάχιστον :min kilobytes.',
        'string' => 'Το πεδίο :attribute πρέπει να έχει μέγεθος τουλάχιστον :min χαρακτήρες.',
        'array' => 'Το πεδίο :attribute πρέπει να έχει τουλάχιστον :min αντικείμενα.',
    ],
    'multiple_of' => 'Το πεδίο :attribute πρέπει να είναι πολλαπλάσιο του :value.',
    'not_in' => 'Η τιμή του πεδίου :attribute είναι άκυρη.',
    'not_regex' => 'Η μορφή της τιμής του πεδίου :attribute είναι άκυρη.',
    'numeric' => 'Το πεδίο :attribute πρέπει να είναι αριθμός.',
    'password' => 'Ο κωδικός είναι άκυρος.',
    'present' => 'Το πεδίο :attribute πρέπει να έχει τιμή.',
    'regex' => 'Η μορφή του πεδίου :attribute είναι άκυρη.',
    'required' => 'Το πεδίο :attribute απαιτείται.',
    'required_if' => 'Το πεδίο :attribute απαιτείται όταν το :other έχει τιμή :value.',
    'required_unless' => 'Το πεδίο :attribute απαιτείται εκτός αν το :other έχει τιμή :values.',
    'required_with' => 'Το πεδίο :attribute απαιτείται όταν έχει τιμή :values.',
    'required_with_all' => 'Το πεδίο :attribute απαιτείται όταν υπάρχει η τιμή :values.',
    'required_without' => 'Το πεδίο :attribute απαιτείται όταν δεν υπάρχει η τιμή :values.',
    'required_without_all' => 'Το πεδίο :attribute απαιτείται όταν δεν υπάρχει καμία από τις τιμές :values.',
    'prohibited' => 'Το πεδίο :attribute απαγορεύεται.',
    'prohibited_if' => 'Το πεδίο :attribute απαγορεύεται όταν το :other έχει τιμή :value.',
    'prohibited_unless' => 'Το πεδίο :attribute απαγορεύεται εκτός αν το :other έχει τιμή :values.',
    'same' => 'Τα πεδία :attribute και :other πρέπει να ταιριάζουν.',
    'size' => [
        'numeric' => 'Το πεδίο :attribute πρέπει να έχει μέγεθος :size.',
        'file' => 'Το αρχείο :attribute πρέπει να έχει μέγεθος :size kilobytes.',
        'string' => 'Το πεδίο :attribute πρέπει να έχει μέγεθος :size χαρακτήρες.',
        'array' => 'Το πεδίο :attribute πρέπει να περιέχει :size αντικείμενα.',
    ],
    'starts_with' => 'Το πεδίο :attribute πρέπει να ξεκινά με ένα από τα ακόλουθα: :values.',
    'string' => 'Το πεδίο :attribute πρέπει να είναι κείμενο.',
    'timezone' => 'Το πεδίο :attribute πρέπει να είναι έγκυρη χρονοζώνη.',
    'unique' => 'Η τιμή του πεδίου :attribute έχει ήδη ληφθεί.',
    'uploaded' => 'Απέτυχε η αποστολή του αρχείου :attribute.',
    'url' => 'Η μορφή του πεδίου :attribute είναι άκυρη.',
    'uuid' => 'Το πεδίο :attribute πρέπει να είναι έγκυρο UUID.',
    'date_multi_format' => 'Το πεδίο :attribute πρέπει να έχει μορφή ημερομηνίας.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        // Ability show
        'person' => 'Επαφή',
        'club' => 'Σωματείο',
        'description' => 'Τίτλος',
        'starting_date' => 'Έναρξη',
        'ending_date' => 'Λήξη',
    ],

];
