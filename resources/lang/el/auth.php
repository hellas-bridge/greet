<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Τα στοιχεία που δώσατε δεν είναι έγκυρα.',
    'password' => 'Ο κωδικός που δώσατε δεν είναι έγκυρος.',
    'throttle' => 'Δοκιμάσατε πολλές φορές να συνδεθείτε. Παρακαλώ δοκιμάστε ξανά σε :seconds δευτερόλεπτα.',

];
