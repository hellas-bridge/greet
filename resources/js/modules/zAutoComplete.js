import autoComplete from "@tarekraafat/autocomplete.js";

/**
 * This is a test
 * @param {object} options Initialization parameters
 * @param {string} options.selector ID of the HTML input element to transform
 * @param {string} options.idSelector ID of the HTML input to hold the ID of the selected item
 * @param {string} options.placeholder Text for the placeholder
 * @param {string} options.ajaxUrl URL to fetch data
 * @param {Array}  options.keys Keys used in search
 * @param {Array}  options.item Options for each item
 * @param {string} options.visibleString String to show on the HTML input element after selection
 * @param {string} options.targetKey JSON data key to fill in the HTML input holding the ID
 * @param {string} options.errorColor Background color of HTML input element when there's text without ID
 */
export default function zAutoComplete(options = {}) {
    let defaults =  {
        selector: '#autoComplete',
        idSelector: null,
        placeholder: 'αναζήτηση...',
        ajaxUrl: '',
        keys: ['name'],
        item: [

        ],
        visibleString: '$(name)',
        targetKey: null,
        errorColor: 'red',
    }
    var options = Object.assign({}, defaults, options)
    
    const autoCompleteJS = new autoComplete({
        wrapper: false,
        selector: options.selector,
        placeHolder: options.placeholder,
        data: {
            src: async (query) => {
                try {
                    const source = await fetch(options.ajaxUrl + encodeURIComponent(`${query}`))
                    const data = await source.json()
                    return data
                } catch (error) {
                    return error
                }
            },
            keys: options.keys,
        },
        resultsList: {
            noResults: false,
            tabSelect: true,
        },
        resultItem: {
            element: options.item,
            highlight: true,
        },
        diacritics: true,
        events: {
            input: {
                focus() {
                    const inputValue = autoCompleteJS.input.value;
                    if (inputValue.length) autoCompleteJS.start();
                },
            },
        },
    })

    setupEventListeners(autoCompleteJS, options)
}

function setupEventListeners(autoCompleteJS, options) {
    if (!options.idSelector) return

    autoCompleteJS.input.addEventListener('selection', function (event) {
        const selection = event.detail.selection
        const val = selection.value
        document.getElementById(options.idSelector).value = selection.value[options.targetKey]
        autoCompleteJS.input.value = options.visibleString(val)
        autoCompleteJS.input.focus()
        autoCompleteJS.input.select()
    });
    
    autoCompleteJS.input.addEventListener('keydown', function (event) {
        const ignored = [
            'Tab', 'Enter', 'NumpadEnter',
            'ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown',
            'Escape',
        ]
        if (!ignored.includes(event.code)) {
            document.getElementById(options.idSelector).value = ''
            autoCompleteJS.input.style.backgroundColor = ''
        }
    })

    autoCompleteJS.input.addEventListener('blur', function bgChange(event) {
        const personID = document.getElementById(options.idSelector)
        if (this.value !== '' && personID.value === '') {
            this.style.backgroundColor = options.errorColor
        } else {
            this.style.backgroundColor = ''
        }
    })
}
