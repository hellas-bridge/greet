document.addEventListener("DOMContentLoaded", () => {
    $.fn.dataTable.ext.errMode = 'none';
    $(function () {
        $('.dataTable').DataTable({
            autoWidth: false,
            processing: false,
            serverSide: true,
            lengthMenu: [ [10, 20, 50], [10, 20, 50] ],
            language: {
                url: '/js/components/datatables.el.json'
            },
            ajax: window.ajaxroute,
            columns: [
                {
                    data: 'last_name',
                    name: 'last_name'
                },
                {
                    data: 'first_name',
                    name: 'first_name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'phone',
                    name: 'phone'
                },
                {
                    data: 'mobile',
                    name: 'mobile'
                },
                {
                    data: 'clubname',
                    name: 'clubs.name'
                },
                {
                    data: 'url',
                    name: 'url',
                    visible: false,
                    searchable: false,
                },
                {
                    data: 'trashed',
                    name: 'trashed',
                    visible: false,
                    searchable: false,
                },
            ],
            createdRow: function(row, data, index) {
                row.setAttribute('data-url', data.url);
                if (data['trashed'] == true) {
                    row.classList.add('bg-warning')
                    row.classList.remove('even')
                    row.classList.remove('odd')
                } else if (index % 2 == 1) {
                    row.style.backgroundColor = 'var(--bs-table-striped-bg)'
                }
            },
            order: [
                [0, 'asc']
            ],
            drawCallback: function (settings) {
                $('.dataTable').removeClass('d-none')
                $('.dataTable tbody tr')
                    .addClass('clicky')
                    .addClass('pointy')
                    .on('click', function () {
                        window.location = $(this).data('url');
                    }
                )
                $('input[type="search"]').trigger('focus')
            }
        });
    });
});
