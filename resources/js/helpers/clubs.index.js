document.addEventListener("DOMContentLoaded", () => {
    document.querySelectorAll('.clicky').forEach(el => {
        el.addEventListener('click', () => {
            window.location = el.dataset['url'];
        });
    });
    $('table.table').DataTable({
        autoWidth: false,
        lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "Όλες"] ],
        language: {
            url: '/js/components/datatables.el.json'
        },
        columnDefs: [
            {
                width: '25%',
                targets: 1
            },
            {
                width: '15%',
                targets: [0, 2, 3, 4]
            },
            {
                type: 'locale-compare', targets: [0]
            },
            {
                className: 'text-center',
                targets: [2, 3, 4, 5],
            }
        ],
        drawCallback: function (settings) {
            $('table.table').removeClass('d-none')
            $('input[type="search"]').trigger('focus')
        }
    });
});
