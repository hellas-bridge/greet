document.addEventListener("DOMContentLoaded", () => {
    $.fn.dataTable.ext.errMode = 'none';
    $(function () {
        $('.dataTable').DataTable({
            autoWidth: false,
            processing: false,
            serverSide: true,
            lengthMenu: [ [10, 25, 50], [10, 25, 50] ],
            language: {
                url: '/js/components/datatables.el.json'
            },
            ajax: window.ajaxroute,
            columns: [
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'people',
                    name: 'people',
                    searchable: false,
                },
                {
                    data: 'clubs',
                    name: 'clubs',
                    searchable: false,
                },
                {
                    data: 'url',
                    name: 'url',
                    visible: false,
                    orderable: false,
                    searchable: false,
                },
            ],
            columnDefs: [
                {
                    width: '50%',
                    targets: [0],
                },
                {
                    width: '25%',
                    targets: [1, 2],
                },
                {
                    className: 'text-center',
                    targets: [1, 2],
                }
            ],
            createdRow: function(row, data, index) {
                row.setAttribute('data-url', data.url);
                if (data['trashed'] == true) {
                    row.classList.add('bg-warning')
                    row.classList.remove('even')
                    row.classList.remove('odd')
                } else if (index % 2 == 1) {
                    row.style.backgroundColor = 'var(--bs-table-striped-bg)'
                }
            },
            order: [
                [0, 'asc']
            ],
            drawCallback: function (settings) {
                $('.dataTable').removeClass('d-none')
                $('.dataTable tbody tr')
                    .addClass('clicky')
                    .addClass('pointy')
                    .on('click', function () {
                        window.location = $(this).data('url');
                    }
                )
                $('input[type="search"]').trigger('focus')
            }
        });
    });
});
