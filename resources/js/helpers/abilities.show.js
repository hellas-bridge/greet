import axios from "axios";
import zAutoComplete from '../modules/zAutoComplete.js'

document.addEventListener("DOMContentLoaded", () => {
    $.fn.dataTable.ext.errMode = 'none';
    
    // datatable
    $(function () {
        var table = $('.dataTable').DataTable({
            autoWidth: false,
            processing: false,
            serverSide: true,
            lengthMenu: [ [10, 25, 50], [10, 25, 50] ],
            language: {
                url: '/js/components/datatables.el.json'
            },
            ajax: window.ajaxroute1,
            columns: [
                {
                    data: 'person',
                    name: 'people.last_name',
                },
                {
                    data: 'club',
                    name: 'clubs.name',
                },
                {
                    data: 'description',
                    name: 'ability_club_person.description',
                },
                {
                    name: 'ability_club_person.starting_date',
                    data: 'starting_date',
                    searchable: false,
                },
                {
                    name: 'ability_club_person.ending_date',
                    data: 'ending_date',
                    searchable: false,
                },
                {
                    data: 'active',
                    searchable: false,
                    render: function (data, type, row) {
                        return data ?
                            '<span data-id="' + row.id + '" class="text-info pointy clicky switch_pivot"><i class="far far fa-check-square"></i></span>' :
                            '<span data-id="' + row.id + '" class="pointy clicky switch_pivot"><i class="far far fa-minus-square"></i></span>'
                    },
                },
                {
                    data: 'id',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row) {
                        return '<span data-id="' + data + '" class="text-danger delete_pivot pointy clicky"><i class="far fa-trash-alt"></i></span>'
                    },
                },
            ],
            columnDefs: [
                {
                    className: 'text-center',
                    targets: [1, 3, 4, 5, 6],
                }
            ],
            createdRow: function(row, data, index) {
                if (index % 2 == 1) {
                    row.style.backgroundColor = 'var(--bs-table-striped-bg)'
                }
            },
            order: [
                [5, 'desc'],
                [0, 'asc'],
            ],
            drawCallback: function (settings) {
                $('.dataTable').removeClass('d-none')
                $('.dataTable tbody tr')

                $('.dataTables_wrapper > div.row div.col-md-6').first()
                    .after('<div class="col-md-4 text-center"><button class="btn py-0 btn-primary reload d-none"><i class="fas fa-sync-alt"></i></button></div>')

                $('.dataTables_wrapper > div.row div.col-md-6').each(function () {
                    $(this).removeClass('col-md-6').addClass('col-md-4')
                })

                // switch active state
                $('span.switch_pivot').on('click', function (e) {
                    e.preventDefault()
                    const id = $(this).data('id')
                    const span = $(this)
                    const tr = $(this).parentsUntil('tbody')[1];
                    axios.patch('/api/ability-club-person/' + id)
                        .then(function (response) {
                            console.log(response.data.result)
                            if (response.data.result === 'error') {
                                $(tr).addClass('row-animation-danger')
                                setTimeout(() => $(tr).removeClass('row-animation-danger'), 200)
                            } else {
                                $('button.reload').removeClass('d-none')
                                response.data.result ?
                                    span.addClass('text-info').html('<i class="far far fa-check-square"></i>') :
                                    span.removeClass('text-info').html('<i class="far far fa-minus-square"></i>')
                                $(tr).addClass('row-animation-normal')
                                setTimeout(() => $(tr).removeClass('row-animation-normal'), 200)
                            }
                        })
                    return false;
                })

                // delete row
                $('span.delete_pivot').on('click', function (e) {
                    e.preventDefault()
                    const id = $(this).data('id')
                    const td = $(this).parentsUntil('tr');
                    const tr = $(this).parentsUntil('tbody')[1];
                    axios.delete('/api/ability-club-person/' + id)
                        .then(function (response) {
                            if (response.data.result === 'error') {
                                $(tr).addClass('row-animation-danger')
                                setTimeout(() => $(tr).removeClass('row-animation-danger'), 200)
                            } else {
                                $('button.reload').removeClass('d-none')
                                $(td).empty()
                                $(td).prev().empty()
                                $(tr).addClass('row-animation-warning')
                                    .delay(180)
                                    .queue(function () {
                                        $(tr).removeClass('row-animation-warning')
                                            .addClass('bg-warning')
                                            .dequeue()
                                    })
                            }
                        })
                    return false;
                })

                // reload button
                $('button.reload').on('click', function (e) {
                    e.preventDefault()
                    settings.oInstance.api().ajax.reload()
                    $(this).addClass('d-none')
                })
            },
            initComplete: function (settings, json) {
                $('input[type="search"]').trigger('focus')
            }
        });

        window.table= table

        // add new record
        $('#addRecord').on("click", function (e) {
            e.preventDefault()
            var row = {
                ability_id: $('#ability_id').text(),
                person_id: $('input#person-id').val(),
                club_id: $('input#club-id').val(),
                active: true,
                description: $('input#description').val(),
                starting_date: $('input#starting_date').val(),
                ending_date: $('input#ending_date').val(),
            }
            axios.post(window.ajaxroute2, row)
                .then((response) => {
                    $('.acp_field').each(function () {
                        $(this).removeClass('is-invalid')
                    })
                    $('#acp_data').empty()

                    // insert new row at the top of the table
                    for (let w = table.rows().data().length-1; w > 0; w--) {
                        table.row(w).data(table.row(w-1).data())
                    }
                    table.row(0).data(response.data.data[0]).draw()

                    // clear fields
                    $('input#person').val('')
                    $('input#club').val('')
                    $('input#description').val('')
                    $('input#starting_date').val('')
                    $('input#ending_date').val('')

                    // light up the new row
                    let tr = table.row(0).nodes().toJQuery()
                    tr.css('background-color', 'lightgreen')

                    // show reload button
                    $('button.reload').removeClass('d-none')

                    // set the focus
                    $('input#person').trigger('focus')
                })
                .catch((error) => {
                    let errors = error.response?.data?.errors

                    // remove old errors
                    $('.acp_field').each(function () {
                        $(this).removeClass('is-invalid')
                    })
                    $('#acp_data').empty()

                    // add new errors
                    if (errors) {
                        for (const [key, value] of Object.entries(errors)) {
                            $('#' + key).addClass('is-invalid')
                            $('#acp_data').append(`<li>${value}</li>`)
                        }
                        $('#acp_errors').removeClass('d-none');
                    } else {
                        $('#acp_data').append(`<li>Γενικό σφάλμα εφαρμογής</li>`)
                        $('#acp_errors').removeClass('d-none');
                        console.log(error)
                    }
                })
        })
    });

    // autocomplete person
    zAutoComplete({
        selector: '#person',
        idSelector: 'person-id',
        placeholder: '',
        ajaxUrl: '/api/search/people/',
        keys: ['name', 'code'],
        item: (item, data) => {
            const itemText = data.key == 'code' ?
                `
            <span class="data-code">${data.match}</span>
            <span class="data-name">${data.value.name}</span>` :
                `
            <span class="data-code">${data.value.code}</span>
            <span class="data-name">${data.match}</span>`
            item.innerHTML = itemText
        },
        visibleString: (val) => {
            if (val.code == '') {
                return `[ # ] ${val.name}`
            }
            return `[ ${val.code} ] ${val.name}`
        },
        targetKey: 'id',
        errorColor: window.theme.danger,
    })

    // autocomplete club
    zAutoComplete({
        selector: '#club',
        idSelector: 'club-id',
        placeholder: '',
        ajaxUrl: '/api/search/clubs/',
        keys: ['name'],
        item: (item, data) => {
            item.innerHTML = `<span class="data-name">${data.value.name}</span>`
        },
        visibleString: (val) => {
            return `${val.name}`
        },
        targetKey: 'id',
        errorColor: window.theme.danger,
    })
});
