import axios from "axios";

// set cookie when sidebar expands / collapses
const initialize = () => {
  const sidebarToggleElement = document.getElementsByClassName("js-sidebar-toggle")[0];

  if(sidebarToggleElement) {
    sidebarToggleElement.addEventListener("click", () => {
      axios.post('/api/sidebar-toggle', {})
      .then(function (response) {
        document.cookie = "sidebar=" + response.data + "; path=/";
      });
    });
  }
}

// Wait until page is loaded
document.addEventListener("DOMContentLoaded", () => initialize());
