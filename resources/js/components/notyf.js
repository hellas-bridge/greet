import { Notyf } from 'notyf';
import 'notyf/notyf.min.css'; // for React, Vue and Svelte

// Create an instance of Notyf
window.notyf = new Notyf({
  duration: 2500,
  position: {
    x: 'center',
    y: 'top'
  },
  types: [
    {
      type: 'warning',
      className: 'notyf__toast--error',
      backgroundColor: '#fcb92c',
      icon: {
        className: 'notyf__icon--error',
        tagName: 'i',
      },
    },
    {
      type: 'info',
      className: 'notyf__toast--success',
      backgroundColor: '#17a2b8',
      icon: {
        className: 'notyf__icon--success',
        tagName: 'i',
      },
    },
    {
      type: 'error',
      background: '#dc3545'
    },
    {
      type: 'success',
      background: '#059669'
    }
  ],
  dismissible: true
});
