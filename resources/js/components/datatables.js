// Datatables
window.$ = window.jQuery = require('jquery');
window.$ = $;
require('datatables.net-bs5');
require('datatables.net-buttons-bs5');
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    'locale-compare-asc': function (a, b) {
        return a.localeCompare(b, 'cs', { sensitivity: 'case' })
    },
    'locale-compare-desc': function (a, b) {
        return b.localeCompare(a, 'cs', { sensitivity: 'case' })
    }
})

jQuery.fn.dataTable.ext.type.search['locale-compare'] = function (data) {
    return NeutralizeAccent(data);
}

function NeutralizeAccent(data) {
    return !data
        ? ''
        : typeof data === 'string'
            ? data
                .replace(/\n/g, ' ')
                .replace(/[ΑαΆά]/g, 'α')
                .replace(/[ΕεΈέ]/g, 'ε')
                .replace(/[ΗηΉή]/g, 'η')
                .replace(/[ΙιΊίΪϊΐ]/g, 'ι')
                .replace(/[ΟοΌό]/g, 'ο')
                .replace(/[ΥυΎύΫϋΰ]/g, 'y')
                .replace(/[ΩωΏώ]/g, 'ω')
                .replace(/[Σσς]/g, 'σ')
            : data
}
