const previewImage = document.querySelector('.image-preview')
const uploadInput  = document.querySelector('.upload-file-hidden')
const hiddenInput  = document.querySelector('#image-hidden-field')
const removeIcon = document.querySelector('.image-remove-icon-minus');
const deleteIcon = document.querySelector('.image-remove-icon-delete')

uploadInput.addEventListener('change', function () {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return;

    if (/^image/.test( files[0].type)) {
        var reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onloadend = function () {
            /* set the background to new image */
            previewImage.style.backgroundImage = "url("+this.result+")";
            /* show remove icon */
            removeIcon.classList.remove("d-none");
            /* hide delete icon */
            deleteIcon.classList.add("d-none");
        }
    }
});

removeIcon.addEventListener('click', function () {
    /* restore original image, if any */
    previewImage.style.backgroundImage =
        "url(" + previewImage.dataset.background + ")";
    /* remove upload value */
    uploadInput.value = "";
    /* hide remove icon */
    removeIcon.classList.add("d-none");
    /* if we had an image, originally */
    if (uploadInput.dataset.value !== '') {
        /* show delete icon */
        deleteIcon.classList.remove("d-none");
    };
    /* set the hidden filed to false (original image must be preserved) */
    hiddenInput.value = "0"
});

deleteIcon.addEventListener('click', function () {
    /* set the background to blank */
    previewImage.style.backgroundImage = null;
    /* hide delete icon */
    deleteIcon.classList.add("d-none");
    /* set the hidden filed to true (original image must be deleted) */
    hiddenInput.value = "1"
});
