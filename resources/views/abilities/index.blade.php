<x-app-layout>
    <x-slot name="title">Ιδιότητες</x-slot>

    <x-slot name="header">Κατάλογος ιδιοτήτων</x-slot>

    <div class="card">
        <table class="dataTable table table-hover table-responsive px-0 d-none">
            <thead>
                <tr>
                    <th>Ονομασία</th>
                    <th>Επαφές</th>
                    <th>Σωματεία</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <x-slot name="extra_scripts">
        <script>window.ajaxroute = '/api/abilities{{ $trashed ? '?trashed' : '' }}';</script>
        <script src="{{ mix('js/components/datatables.js') }}" defer></script>
        <script src="{{ mix('js/helpers/abilities.index.js') }}" defer></script>
    </x-slot>

    <x-slot name="toolbar">
        <div class="d-flex align-items-stretch justify-content-between">
            <x-tool href="{{ route('abilities.index') }}{{ $trashed ? '' : '?trashed' }}"
                icon="{{ $trashed ? 'far fa-trash-alt' : false }}"
                type="{{ $trashed ? 'btn-warning' : false }}">
                {{ $trashed ? 'όλες' : 'ενεργές' }}
            </x-tool>
            <x-tool href="{{ route('abilities.create') }}"
                icon="far fa-file"
                type="btn-primary">
                προσθήκη
            </x-tool>
        </div>
    </x-slot>
</x-app-layout>
