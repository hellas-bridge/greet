<x-app-layout>
    <x-slot name="title">{{ $ability->name }}</x-slot>

    <x-slot name="header">
        Ιδιότητα: {{ $ability->name }}
    </x-slot>

    <div class="row row-cols-2 g-2">
    </div>

    <x-slot name="toolbar">
        <div class="d-flex align-items-stretch justify-content-between">
            <div class="btn-group">
                <x-tool href="{{ route('abilities.show', $ability) }}"
                    icon="fas fa-angle-left" />
            </div>
        </div>
    </x-slot>
</x-app-layout>
