<x-app-layout>
    <x-slot name="title">{{ $ability->name }}</x-slot>

    <x-slot name="header">
        Ιδιότητα: <strong>{{ $ability->name }}</strong>
    </x-slot>

    <div class="card">
        <div class="d-none" id="acp_errors">
            <div class="text-danger">
                Ουπς! Κάτι πήγε στραβά!
            </div>

            <ul class="text-danger" id="acp_data">
            </ul>
        </div>
        <h4 class="bg-cyan text-light rounded-top p-3">
            <span id="ability_id" class="d-none">{{ $ability->id }}</span>
            {{ $ability->name }}
        </h4>
        <div class="card-body">
            <h5>Εισαγωγή ιδιότητας</h5>
            <div class="row align-items-center gx-1">
                <div class="col col-sm-2">
                    <div class="autoComplete_wrapper">
                        <x-input
                            name="person"
                            type="text"
                            label="Επαφή"
                            class="acp_field"
                            />
                        <input type="hidden" id="person-id" name="person-id" value="" disabled>

                    </div>
                </div>
                <div class="col col-sm-2">
                    <div class="autoComplete_wrapper">
                        <x-input
                            name="club"
                            type="text"
                            label="Σωματείο"
                            class="acp_field"
                            />
                        <input type="hidden" id="club-id" name="club-id" value="" disabled>
                    </div>
                </div>
                <div class="col col-sm-3">
                    <x-input
                    name="description"
                    type="text"
                    label="Τίτλος"
                    class="acp_field"
                    />
                </div>
                <div class="col col-sm-2">
                    <x-input
                    name="starting_date"
                    type="text"
                    label="Έναρξη"
                    class="acp_field"
                    />
                </div>
                <div class="col col-sm-2">
                    <x-input
                    name="ending_date"
                    type="text"
                    label="Λήξη"
                    class="acp_field"
                    />
                </div>
                <div class="col col-sm-1 text-center pb-3">
                    <x-button id="addRecord" class="btn-primary">Οκ</x-button>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <table class="dataTable table table-hover table-responsive px-0 d-none">
            <thead>
                <tr>
                    <th>Επαφή</th>
                    <th>Σωματείο</th>
                    <th>Τίτλος</th>
                    <th>(Έναρξη)</th>
                    <th>(Λήξη)</th>
                    <th>(Ενεργή)</th>
                    <th><i class="far fa-trash-alt"></i></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <x-slot name="extra_scripts">
        <script>window.ajaxroute1 = '/api/abilities/{{ $ability->id }}';</script>
        <script>window.ajaxroute2 = '/api/ability-club-person';</script>
        <script src="{{ mix('js/components/datatables.js') }}" defer></script>
        <script src="{{ mix('js/helpers/abilities.show.js') }}" defer></script>
    </x-slot>

    <x-slot name="toolbar">
        <div class="d-flex align-items-stretch justify-content-between">
            <div class="btn-group">
                <x-tool href="{{ route('abilities.index') }}"
                    icon="fas fa-angle-left" />
                <x-tool href="{{ route('abilities.edit', ['ability' => $ability]) }}"
                    icon="fas fa-pen"
                    type="btn-primary">
                    αλλαγή
                </x-tool>
            </div>
            <div class="btn-group">
                <form method="POST" action="{{ route('abilities.destroy', ['ability' => $ability]) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <x-tool
                        icon="fas fa-trash"
                        type="btn-danger">
                        διαγραφή
                    </x-tool>
                </form>
            </div>
        </div>
    </x-slot>
</x-app-layout>
