@if ($message = Session::get('success'))
<!-- flash message -->
<script>
    document.addEventListener("DOMContentLoaded", () => {
        window.notyf.open({
            type: 'success',
            message: {!! json_encode($message) !!}
        });
    });
</script>
@endif

@if ($message = Session::get('error'))
<!-- flash message -->
<script>
    document.addEventListener("DOMContentLoaded", () => {
        window.notyf.open({
            type: 'error',
            message: {!! json_encode($message) !!}
        });
    });
</script>
@endif

@if ($message = Session::get('warning'))
<!-- flash message -->
<script>
    document.addEventListener("DOMContentLoaded", () => {
        window.notyf.open({
            type: 'warning',
            message: {!! json_encode($message) !!}
        });
    });
</script>
@endif

@if ($message = Session::get('info'))
<!-- flash message -->
<script>
    document.addEventListener("DOMContentLoaded", () => {
        window.notyf.open({
            type: 'info',
            message: {!! json_encode($message) !!}
        });
    });
</script>
@endif

@if ($errors->any())
@endif
