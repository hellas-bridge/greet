<x-guest-layout>

    <!-- logo -->
    <div class="text-center mt-4">
        <a href="/" tabindex="-1">
            <x-application-logo class="logo-square" />
        </a>
    </div>

    <!-- form -->
    <div class="card mt-4">
        <div class="card-body">
            <div class="m-sm-4">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <!-- form -->
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <!-- Password Reset Token -->
                    <input type="hidden" name="token" value="{{ $request->route('token') }}">

                    <!-- Email -->
                    <x-input
                        name="email"
                        type="email"
                        label="Email"
                        value="{{ old('email', $request->email) }}"
                        required />

                    <!-- Password -->
                    <x-input
                        name="password"
                        type="password"
                        label="Νέος κωδικός"
                        required
                        autofocus
                        autocomplete="new-password" />

                    <!-- Confirm Password -->
                    <x-input
                        name="password_confirmation"
                        type="password"
                        label="Επιβεβαίωση Κωδικού"
                        required />

                    <!-- Reset -->
                    <div class="text-center">
                        <x-button>
                            Επαναφορά Κωδικού
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
