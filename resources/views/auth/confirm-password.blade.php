<x-guest-layout>

    <!-- logo -->
    <div class="text-center mt-4">
        <a href="/" tabindex="-1">
            <x-application-logo class="logo-square" />
        </a>
    </div>

    <!-- form -->
    <div class="card mt-4">
        <div class="card-body">
            <div class="m-sm-4">
                <!-- Label -->
                <p class="mb-4">
                     Αυτή είναι μια ασφαλής περιοχή της εφαρμογής. Παρακαλούμε επιβεβαιώστε τον κωδικό σας.
                </p>

                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <!-- form -->
                <form method="POST" action="{{ route('password.confirm') }}">
                    @csrf

                    <!-- Password -->
                    <x-input
                        name="password"
                        type="password"
                        label="Κωδικός"
                        required
                        autofocus
                        autocomplete="current-password" />

                    <!-- Confirm -->
                    <div class="text-center">
                        <x-button>
                            Επιβεβαίωση
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-guest-layout>
