<x-guest-layout>

    <!-- logo -->
    <div class="text-center mt-4">
        <a href="/" tabindex="-1">
            <x-application-logo class="logo-square" />
        </a>
    </div>

    <!-- form -->
    <div class="card mt-4">
        <div class="card-body">
            <div class="m-sm-4">
                <!-- Label -->
                <p>
                    Ευχαριστούμε για την εγγραφή σας. Πριν ξεκινήσετε, παρακαλούμε επιβεβαιώστε την email διεύθυνσή σας, κάνοντας κλικ στον σύνδεσμο που σας στείλαμε. Αν δε λάβατε το μήνυμα, ευχαρίστως θα σας στείλουμε ένα νέο.
                </p>

                @if (session('status') == 'verification-link-sent')
                <!-- Session -->
                    <p class="text-success mb-4">
                        Ένας νέος σύνδεσμος επιβεβαίωσης εστάλη στην email διεύθυνση που μας δώσατε κατά την εγγραφή σας.
                    </p>
                @endif

                <!-- form -->
                <form method="POST" action="{{ route('verification.send') }}">
                    @csrf

                    <!-- Confirm -->
                    <div class="text-center">
                        <x-button>
                            Αποστολή νέου μηνύματος επιβεβαίωσης
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-guest-layout>
