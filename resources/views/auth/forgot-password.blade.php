<x-guest-layout>

    <!-- logo -->
    <div class="text-center mt-4">
        <a href="/" tabindex="-1">
            <x-application-logo class="logo-square" />
        </a>
    </div>

    <!-- form -->
    <div class="card mt-4">
        <div class="card-body">
            <div class="m-sm-4">
                <!-- Label -->
                <p>
                    Ξεχάσατε τον κωδικό σας; Γράψτε το email σας και θα σας στείλουμε ένα μήνυμα, που θα περιέχει έναν σύνδεσμο για δημιουργία νέου κωδικού.
                </p>

                <!-- Session Status -->
                <x-auth-session-status :status="session('status')" />

                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <!-- form -->
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <!-- Email Address -->
                    <div class="form-floating mb-4">
                        <x-input
                            name="email"
                            label="Email"
                            value="{{ old('email') }}"
                            required
                            autofocus />
                    </div>

                    <!-- Send Link -->
                    <div class="text-center">
                        <x-button>
                            Αποστολή συνδέσμου
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-guest-layout>
