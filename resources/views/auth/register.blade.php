<x-guest-layout>

    <!-- logo -->
    <div class="text-center mt-4">
        <a href="/" tabindex="-1">
            <x-application-logo class="logo-square" />
        </a>
    </div>

    <!-- form -->
    <div class="card mt-4">
        <div class="card-body">
            <div class="m-sm-4">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <!-- form -->
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <!-- Name -->
                    <x-input
                        name="name"
                        label="Ονοματεπώνυμο"
                        value="{{ old('name') }}"
                        required
                        autofocus
                        autocomplete="name" />

                    <!-- Username -->
                    <x-input
                        name="username"
                        label="Ψευδώνυμο"
                        value="{{ old('username') }}"
                        required
                        autocomplete="username" />

                    <!-- Email -->
                    <x-input
                        name="email"
                        type="email"
                        label="Email"
                        value="{{ old('email') }}"
                        required />

                    <!-- Password -->
                    <x-input
                        name="password"
                        type="password"
                        label="Κωδικός"
                        required
                        autocomplete="new-password" />

                    <!-- Confirm Password -->
                    <x-input
                        name="password_confirmation"
                        type="password"
                        label="Επιβεβαίωση Κωδικού"
                        required
                        autocomplete="new-password" />

                    <div class="row justify-content-between">
                        <div class="col-5">
                            <x-button class="btn-success" href="{{ route('login') }}" tabindex="-1">
                                Ήδη εγγεγραμμένος;
                            </x-button>
                        </div>
                        <div class="col-5 text-end">
                            <x-button>
                                Εγγραφή
                            </x-button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
