<x-guest-layout>

    <!-- logo -->
    <div class="text-center mt-4">
        <a href="/" tabindex="-1">
            <x-application-logo class="logo-square" />
        </a>
    </div>

    <!-- form -->
    <div class="card mt-4">
        <div class="card-body">
            <div class="m-sm-4">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <!-- form -->
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <!-- Login Field -->
                    <x-input
                        name="login"
                        label="Ψευδώνυμο ή Email"
                        value="{{ old('login') }}"
                        tabindex="1"
                        required
                        autofocus
                        autocomplete="email" />

                    <!-- Password -->
                    <x-input
                        name="password"
                        type="password"
                        label="Κωδικός"
                        value="{{ old('login') }}"
                        tabindex="2"
                        required
                        autocomplete="current-password"
                        small="<a href='{{ route('password.request') }}' class='link-secondary'>Ξεχάσατε τον κωδικό σας;</a>" />

                    <!-- Remember Me -->
                    <x-input-checkbox
                        name="remember"
                        type="checkbox"
                        label="Να με θυμάσαι"
                        labelClass="form-check-label"
                        tabindex="3" />

                    <!-- Login -->
                    <div class="row justify-content-between">
                        <div class="col-5">
                            <x-button class="btn-secondary" href="{{ route('register') }}" tabindex="-1">
                                Εγγραφή
                            </x-button>
                        </div>
                        <div class="col-5 text-end">
                            <x-button tabindex="4">
                                Σύνδεση
                            </x-button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
