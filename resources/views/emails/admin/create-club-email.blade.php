@component('mail::message')
# Ενημέρωση

Πρέπει να δημιουργηθεί / τροποποιηθεί ένα email για το σωματείο **#{{ $club->id }} {{ $club->name }}**.

### {{ $club->email }}

Ευχαριστούμε,<br>
{{ config('app.name') }}
@endcomponent
