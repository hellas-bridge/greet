<x-app-layout>
    <x-slot name="title">{{ $person->id ? 'Επεξεργασία επαφής ' . $person->last_name : 'Νέα επαφή' }}</x-slot>

    <x-slot name="header">
        {{ $person->id ? 'Επεξεργασία επαφής ' . $person->last_name : 'Νέα επαφή' }}
    </x-slot>

    <!-- Validation Errors -->
    <x-auth-validation-errors class="mb-4" :errors="$errors" />

    <!-- form -->
    <form method="POST" enctype=multipart/form-data action="{{ $person->id ? route('people.update', ['person' => $person ]) : route('people.store') }}">
        @csrf
        {{ $person->id ? method_field('PUT') : '' }}
        <div class="row row-cols-1 g-2 mb-4 d-print-none">
            <!-- Γενικά Στοιχεία -->
            <div class="col-md-12">
                <div class="card">
                    <h4 class="bg-cyan text-light rounded-top p-3">Γενικά στοιχεία</h4>
                    <div class="card-body">
                        <div class="card-text">
                            <!-- Όνομα -->
                            <x-input
                                name="first_name"
                                type="text"
                                label="Όνομα"
                                value="{{ old('first_name') ?: $person->first_name }}"
                                autofocus
                            />
                            <!-- Επώνυμο -->
                            <x-input
                                name="last_name"
                                type="text"
                                required
                                label="Επώνυμο"
                                value="{{ old('last_name') ?: $person->last_name }}"
                            />
                            <!-- Αρχικό -->
                            <x-input
                                name="initial"
                                type="text"
                                label="Αρχικό"
                                value="{{ old('initial') ?: $person->initial }}"
                            />
                            <!-- Φύλλο -->
                            <x-input-radio-sex
                                name="male"
                                value="{{ old('active') ?: $person->male }}"
                            />
                            <!-- Γέννηση -->
                            <x-input
                                name="birth_date"
                                type="text"
                                label="Γέννηση"
                                value="{{ old('birth_date') ?: ($person->birth_date ? $person->birth_date->format('d/m/Y') : '') }}"
                            />
                            <!-- Τηλέφωνο -->
                            <x-input
                                name="phone"
                                type="text"
                                label="Τηλέφωνο"
                                value="{{ old('phone') ?: $person->phone }}"
                            />
                            <!-- Κινητό -->
                            <x-input
                                name="mobile"
                                type="text"
                                label="Κινητό"
                                value="{{ old('mobile') ?: $person->mobile }}"
                            />
                            <!-- Email -->
                            <x-input
                                name="email"
                                type="email"
                                label="Email"
                                value="{{ old('email') ?: $person->email }}"
                            />
                            <!-- Πατρώνυμο -->
                            <x-input
                                name="fathers_name"
                                type="text"
                                label="Πατρώνυμο"
                                value="{{ old('fathers_name') ?: $person->fathers_name }}"
                            />
                            <!-- Επάγγελμα -->
                            <x-input
                                name="profession"
                                type="text"
                                label="Επάγγελμα"
                                value="{{ old('profession') ?: $person->profession }}"
                            />
                            <!-- Ταυτότητα -->
                            <x-input
                                name="identity_card"
                                type="text"
                                label="Αρ. Ταυτ."
                                value="{{ old('identity_card') ?: $person->identity_card }}"
                            />
                            <!-- Φωτογραφία -->
                            <x-input-image
                                name="image"
                                label="Εικόνα"
                                value="{{ $person->imageURL }}"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-print-none d-flex align-items-stretch justify-content-between">
            <x-button class="btn-primary">{{ $person->id ? 'Ενημέρωση' : 'Αποθήκευση' }}</x-button>
        </div>
    </form>

    <x-slot name="toolbar">
        <div class="btn-group">
            <x-tool href="{{ $person->id ? route('people.show', ['person' => $person]) : route('people.index') }}"
                icon="fas fa-angle-left" />
        </div>
    </x-slot>
</x-app-layout>
