<x-app-layout>
    <x-slot name="title">{{ $person->last_name }}</x-slot>

    <x-slot name="header">
        Καρτέλα: {{ $person->last_name }}
    </x-slot>

    <div class="row row-cols-2 g-2">
        <!-- Γενικά Στοιχεία -->
        <div class="col col-content">
            <div class="card col-content">
                <h4 class="bg-cyan text-light rounded-top p-3">Γενικά στοιχεία</h4>
                <div class="card-body">
                    <div class="card-text">
                        <div class="row mb-4 text-center">
                            <div class="col-12">
                                <img src="{{ $person->imageURL }}" class="img-fluid rounded-circle mb-1" width="128" height="128" />
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Όνομα</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $person->first_name }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Επώνυμο</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $person->last_name }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Αρχικό</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $person->initial }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Φύλλο/Ηλικία</h6>
                            </div>
                            <div class="col-9 text-secondary">
                                {{ $person->male ? 'άνδρας' : 'γυναίκα' }}
                                {{ $person->age ? $person->age . ' ετών' : '' }}
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Τηλέφωνο</h6>
                            </div>
                            <div class="col-9 text-secondary"><a href="tel:{{ $person->phone }}">{{ $person->phone }}</a></div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Κινητό</h6>
                            </div>
                            <div class="col-9 text-secondary"><a href="tel:{{ $person->mobile }}">{{ $person->mobile }}</a></div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Email</h6>
                            </div>
                            <div class="col-9 text-secondary">
                                <a href="mailto:{{ $person->full_name }}<{{ $person->email }}>" target="_blank">{{ $person->email }}</a>
                                @if($person->email)<i class="text-sm fas fa-external-link-alt text-gray-400"></i>@endif

                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Πατρώνυμο</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $person->fathers_name }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Επάγγελμα</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $person->profession }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Αρ. Ταυτ.</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $person->identity_card }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dummy -->
        <div class="col col-content">
            <div class="card col-content">
                <h4 class="bg-gray-300 text-secondary rounded-top p-3">Σημειώσεις</h4>
                <div class="card-body">
                    <div class="card-text">

                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row row-cols-2 g-2 row mb-4">
        <!-- Αθλητικά στοιχεία -->
        <div class="col">
            <div class="card flex">
                <div
                    class="{{ $person->athlete ? 'bg-cyan text-white' : 'bg-gray-300 text-secondary' }} rounded-top p-3 d-flex justify-content-between"
                    data-bs-toggle="collapse"
                    href="#collapseAthleticData"
                    role="button"
                >
                    <h4>Αθλητικά Στοιχεία</h4>
                    <i class="fas fa-chevron-down"></i>
                </div>
                <div class="card-body collapse" id="collapseAthleticData">
                    <div class="card-text">
                    @if($person->athlete)
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Μητρώο</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ codeFormat($person->athlete->code) }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Σωματείο</h6>
                            </div>
                            <div class="col-9 text-secondary">
                            @if ($person->athlete->club)
                                <a href="{{ route('clubs.show', $person->athlete->club) }}">{{ $person->athlete->club->name }}</a>
                            @endif
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Εγγραφή</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $person->athlete->subscription ? $person->athlete->subscription->format('d/m/Y') : '' }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Ανανέωση</h6>
                            </div>
                            <div class="col-9 text-secondary">
                            @if($person->athlete->update)
                                {{ $person->athlete->update->format('d/m/Y') }}
                                {!! $person->athlete->updateColor !!}
                            @endif
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Κατηγορία</h6>
                            </div>
                            <div class="col-9 text-secondary">
                            @if ($person->athlete->category)
                                {{ $person->athlete->category }}
                                - {{ categoryName($person->athlete->category) }}
                            @endif
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Μαύροι</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ numberFormat($person->athlete->black) }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Χρυσοί</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ numberFormat($person->athlete->gold) }}</div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Πλατινένιοι</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ numberFormat($person->athlete->platinum, 1) }}</div>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col">
            <!-- Dummy -->
            <div class="card flex">
                <div
                    class="bg-gray-300 text-secondary rounded-top p-3 d-flex justify-content-between"
                    data-bs-toggle="collapse"
                    href="#collapseDummy"
                    role="button"
                >
                    <h4>Dummy</h4>
                    {{-- <x-icon.chevron-down /> --}}
                </div>
                <div class="card-body collapse" id="collapseDummy">
                    <div class="card-text">

                    </div>
                </div>
            </div>

        </div>

    </div>

    <x-slot name="toolbar">
        <div class="d-flex align-items-stretch justify-content-between">
            <div class="btn-group">
                <x-tool href="{{ route('people.index') }}"
                    icon="fas fa-angle-left" />
                <x-tool href="{{ route('people.edit', ['person' => $person]) }}"
                    icon="fas fa-pen"
                    type="btn-primary">
                    αλλαγή
                </x-tool>
            </div>
            <div class="btn-group">
                <form method="POST" action="{{ route('people.destroy', ['person' => $person]) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <x-tool
                        icon="fas fa-trash"
                        type="btn-danger">
                        διαγραφή
                    </x-tool>
                </form>
            </div>
        </div>
    </x-slot>
</x-app-layout>
