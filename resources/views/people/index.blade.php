<x-app-layout>
    <x-slot name="title">Επαφές</x-slot>

    <x-slot name="header">Κατάλογος επαφών</x-slot>

    <div class="card">
        <table class="dataTable table table-hover table-responsive px-0 d-none">
            <thead>
                <tr>
                    <th>Επώνυμο</th>
                    <th>Όνομα</th>
                    <th>Email</th>
                    <th>Τηλέφωνο</th>
                    <th>Κινητό</th>
                    <th>Υπεύθυνος</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <x-slot name="toolbar">
        <div class="d-flex align-items-stretch justify-content-between">
            <x-tool href="{{ route('people.index') }}{{ $trashed ? '' : '?trashed' }}"
                icon="{{ $trashed ? 'far fa-trash-alt' : false }}"
                type="{{ $trashed ? 'btn-warning' : false }}">
                {{ $trashed ? 'όλες' : 'ενεργές' }}
            </x-tool>
            <x-tool href="{{ route('people.create') }}"
                icon="far fa-file"
                type="btn-primary">
                προσθήκη
            </x-tool>
        </div>
    </x-slot>

    <x-slot name="extra_scripts">
        <script>window.ajaxroute = '/api/people{{ $trashed ? '?trashed' : '' }}';</script>
        <script src="{{ mix('js/components/datatables.js') }}" defer></script>
        <script src="{{ mix('js/helpers/people.index.js') }}" defer></script>
    </x-slot>
</x-app-layout>
