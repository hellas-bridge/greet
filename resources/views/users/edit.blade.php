<x-app-layout>
    <x-slot name="title">Προφίλ</x-slot>

    <x-slot name="header">Επεξεργασία προφίλ</x-slot>

    <!-- form -->
    <div class="row">
        <div class="card">
            <div class="card-body">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <!-- form -->
                <form method="POST" action="{{ route('users.update', ['user' => $user ]) }}">
                    @csrf
                    {{ method_field('PUT') }}

                    <!-- Name -->
                    <x-input
                        name="name"
                        type="text"
                        label="Όνοματεπώνυμο"
                        required
                        value="{{ old('name') ?: $user->name }}"
                        autofocus
                        autocomplete="name" />

                    <!-- Username -->
                    <x-input
                        name="username"
                        type="text"
                        label="Ψευδώνυμο"
                        required
                        value="{{ old('username') ?: $user->username }}"
                        autocomplete="username" />

                    <!-- Email -->
                    <x-input
                        name="email"
                        type="email"
                        label="Email"
                        required
                        value="{{ old('email') ?: $user->email }}"
                        autocomplete="email" />

                    <hr />

                    <h3 class="mb-4">Αλλαγή κωδικού</h3>

                    <!-- New Password -->
                    <x-input
                        name="password"
                        type="password"
                        label="Νέος κωδικός"
                        autocomplete="new-password" />

                    <!-- Confirm New Password -->
                    <x-input
                        name="password_confirmation"
                        type="password"
                        label="Επιβεβαίωση Κωδικού"
                        autocomplete="new-password" />


                    <!-- Save -->
                    <x-button>
                        Ενημέρωση
                    </x-button>
                </form>
            </div>
        </div>
    </div>

</x-app-layout>
