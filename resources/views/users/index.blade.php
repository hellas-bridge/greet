<x-app-layout>
    <x-slot name="title">Χρήστες</x-slot>

    <x-slot name="header">Κατάλογος χρηστών</x-slot>

    <div class="row">
        <div class="card">
            <div class="card-body">
                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>Ψευδώνυμο</th>
                            <th>Όνομα</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr data-url="{{ route('users.show', ['user' => $user]) }}" class="clicky pointy">
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            document.querySelectorAll('.clicky').forEach(el => {
                el.addEventListener('click', () => {
                    window.location = el.dataset['url'];
                });
            });
        });
    </script>

</x-app-layout>
