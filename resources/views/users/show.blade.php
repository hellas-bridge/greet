<x-app-layout>
    <x-slot name="title">Προφίλ</x-slot>

    <x-slot name="header">Προφίλ χρήστη</x-slot>

    <div class="row">
        <div class="card">
            <div class="card-body">
                <!-- Name -->
                <p class="mb-0 pb-0">Ονοματεπώνυμο</p>
                <h4 class="mb-4">{{ $user->name }}</h4>

                <!-- Username -->
                <p class="mb-0 pb-0">Ψευδώνυμο</p>
                <h4 class="mb-4">{{ $user->username }}</h4>

                <!-- Email -->
                <p class="mb-0 pb-0">Email</p>
                <h4 class="mb-4">{{ $user->email }}</h4>

                @if (Auth::user() == $user)
                    <x-button href="{{ route('users.edit', ['user' => $user])}}">
                        Επεξεργασία προφίλ
                    </x-button>
                @endif
            </div>
        </div>
    </div>

</x-app-layout>
