<x-app-layout>
    <x-slot name="title">{{ $club->name }}</x-slot>

    <x-slot name="header">
        Καρτέλα: {{ $club->name }}
    </x-slot>

    <div class="row row-cols-2 g-2">
        <!-- Γενικά Στοιχεία -->
        <div class="col">
            <div class="card">
                <h4 class="bg-cyan text-light rounded-top p-3">Γενικά στοιχεία</h4>
                <div class="card-body">
                    <div class="card-text">
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Όνομα</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->full_name }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Email</h6>
                            </div>
                            <div class="col-9 text-secondary">
                                <a href="mailto:{{ $club->name }}<{{ $club->email }}>" target="_blank">{{ $club->email }}</a>
                                <i class="text-sm fas fa-external-link-alt text-gray-400"></i>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Ιστοχώρος</h6>
                            </div>
                            <div class="col-9 text-secondary align-baseline">
                                <a href="{{ $club->website }}" target="_blank">{{ $club->website }}</a>
                                @if($club->website)<i class="text-sm fas fa-external-link-alt text-gray-400"></i>@endif
                            </div>
                        </div>
                        <hr />
                        <div class="row mb-4">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Ενεργό</h6>
                            </div>
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Πλήρες μέλος</h6>
                            </div>
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Αθηναϊκό</h6>
                            </div>
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Αθ/Θεσ</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <x-check-icon value="{{ $club->active }}" />
                            </div>
                            <div class="col-3">
                                <x-check-icon value="{{ $club->normal_member }}" />
                            </div>
                            <div class="col-3">
                                <x-check-icon value="{{ $club->in_athens }}" />
                            </div>
                            <div class="col-3">
                                <x-check-icon value="{{ $club->in_major_city }}" />
                            </div>
                        </div>
                        <hr />
                    </div>
                </div>
            </div>
        </div>

        <!-- Επικοινωνία -->
        <div class="col">
            <div class="card">
                <h4 class="bg-gray-300 text-secondary rounded-top p-3">Επικοινωνία</h4>
                <div class="card-body">
                    <div class="card-text">
                        <!-- Υπεύθυνος -->
                        @if ($club->manager)
                        <div class="row mb-4">
                            <div class="col-1 text-center">
                                <span class="align-bottom d-block"><i class="fas fa-user text-cyan"></i></span>
                                <span class="align-bottom d-block mt-2"><i class="fas fa-phone"></i></span>
                                <span class="align-bottom d-block mt-2"><i class="fas fa-mobile"></i></span>
                                <span class="align-bottom d-block mt-2"><i class="fas fa-envelope"></i></span>
                        </div>
                            <div class="col-11">
                                <span class="align-bottom d-block"><a href="{{ route('people.show', $club->manager) }}">{{ $club->manager->full_name }}</a>&nbsp;</span>
                                <span class="align-bottom d-block mt-2"><a href="tel:{{ $club->manager->phone }}">{{ $club->manager->phone }}</a>&nbsp;</span>
                                <span class="align-bottom d-block mt-2"><a href="tel:{{ $club->manager->mobile }}">{{ $club->manager->mobile }}</a>&nbsp;</span>
                                <span class="align-bottom d-block mt-2">
                                    <a href="mailto:{{ $club->manager->full_name }}<{{ $club->manager->email }}>" target="_blank">{{ $club->manager->email }}</a>
                                    @if($club->manager->email)<i class="text-sm fas fa-external-link-alt text-gray-400"></i>@endif
                                </span>
                            </div>
                        </div>
                        @endif
                        <!-- Διευθύνσεις -->
                        @if ($club->headquarters())
                        <div class="row mb-4">
                            <div class="col-1 text-center">
                                <i class="fas fa-map-marker text-cyan"></i>
                            </div>
                            <div class="col-11">
                                {!! nl2br($club->headquarters()->toMultiline()) !!}
                            </div>
                        </div>
                        @endif
                        @foreach ($club->nonHeadquarters() as $item)
                        <div class="row mb-4">
                            <div class="col-1 text-center">
                                <i class="fas fa-map-marker"></i>
                            </div>
                            <div class="col-11">
                                {!! nl2br($item->toMultiline()) !!}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-cols-2 g-2 row mb-4">
        <!-- EOM -->
        <div class="col">
            <div class="card">
                <h4 class="bg-gray-300 text-secondary rounded-top p-3">Στοιχεία ΕΟΜ</h4>
                <div class="card-body">
                    <div class="card-text">
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Ίδρυση</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->founded ? $club->founded->format('d/m/Y') : '' }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Καταστατικό</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->bylaws }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Εγγραφή</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->subscription_date ? $club->subscription_date->format('d/m/Y') : '' }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Πρωτόκολλο</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->subscription_protocol }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Μέλος</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->membership_date ? $club->membership_date->format('d/m/Y') : '' }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Πρωτόκολλο</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->membership_protocol }}</div>
                        </div>
                        <hr />
                    </div>
                </div>
            </div>
        </div>

        <div class="col">
            <!-- Εφορία -->
            <div class="card">
                <h4 class="bg-gray-300 text-secondary rounded-top p-3">Στοιχεία Εφορίας</h4>
                <div class="card-body">
                    <div class="card-text">
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">ΑΦΜ</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->irs_vat_number }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Στοιχεία Εφορίας</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->irs_location }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Έδρα</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->irs_headquarters }}</div>
                        </div>
                        <hr />
                    </div>
                </div>
            </div>

            <!-- ΓΓΑ -->
            <div class="card mb-0">
                <h4 class="bg-gray-300 text-secondary rounded-top p-3">Στοιχεία ΓΓΑ</h4>
                <div class="card-body">
                    <div class="card-text">
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Κωδικός</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->gga_code }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Πρωτόκολλο</h6>
                            </div>
                            <div class="col-9 text-secondary">{{ $club->gga_protocol }}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3">
                                <h6 class="mb-0 text-muted">Αναγνωρισμένο</h6>
                            </div>
                            <div class="col-9 text-secondary"><x-check-icon value="{{ $club->gga_recognized }}" /></div>
                        </div>
                        <hr />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="toolbar">
        <div class="d-flex align-items-stretch justify-content-between">
            <div class="btn-group">
                <x-tool href="{{ route('clubs.index') }}"
                    icon="fas fa-angle-left" />
                <x-tool href="{{ route('clubs.edit', ['club' => $club]) }}"
                    icon="fas fa-pen"
                    type="btn-primary">
                    αλλαγή
                </x-tool>
            </div>
            <div class="btn-group">
                <form method="POST" action="{{ route('clubs.destroy', ['club' => $club]) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <x-tool
                        icon="fas fa-trash"
                        type="btn-danger">
                        διαγραφή
                    </x-tool>
                </form>
            </div>
        </div>
    </x-slot>
</x-app-layout>
