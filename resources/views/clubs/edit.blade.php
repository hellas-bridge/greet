<x-app-layout>

    <x-slot name="title">{{ $club->id ? 'Επεξεργασία σωματείου ' . $club->name : 'Νέο σωματείο' }}</x-slot>

    <x-slot name="header">
        {{ $club->id ? 'Επεξεργασία σωματείου ' . $club->name : 'Νέο σωματείο' }}
    </x-slot>

    <!-- Validation Errors -->
    <x-auth-validation-errors class="mb-4" :errors="$errors" />



    <!-- form -->
    <form method="POST" action="{{ $club->id ? route('clubs.update', ['club' => $club ]) : route('clubs.store') }}">
        @csrf
        {{ $club->id ? method_field('PUT') : '' }}
        <div class="row row-cols-2 g-2 mb-4 d-print-none">
            <!-- Γενικά Στοιχεία -->
            <div class="col">
                <div class="card">
                    <h4 class="bg-cyan text-light rounded-top p-3">Γενικά στοιχεία</h4>
                    <div class="card-body">
                        <div class="card-text">
                            <!-- Όνομα -->
                            <x-input
                                name="name"
                                type="text"
                                label="Όνομα"
                                required
                                value="{{ old('name') ?: $club->name }}"
                                autofocus
                            />
                            <!-- Πλήρες Όνομα -->
                            <x-input
                                name="full_name"
                                type="text"
                                label="Πλήρες Όνομα"
                                value="{{ old('full_name') ?: $club->full_name }}"
                            />
                            <!-- Email -->
                            <x-input
                                name="email"
                                type="email"
                                label="Email"
                                value="{{ old('email') ?: $club->email }}"
                            />
                            <!-- Ιστοσελίδα -->
                            <x-input
                                name="website"
                                type="text"
                                label="Ιστοχώρος"
                                value="{{ old('website') ?: $club->website }}"
                                autofocus
                            />
                            <div class="row mb-4">
                                <div class="col-3">
                                    <h6 class="mb-0">Ενεργό</h6>
                                </div>
                                <div class="col-3">
                                    <h6 class="mb-0">Πλήρες μέλος</h6>
                                </div>
                                <div class="col-3">
                                    <h6 class="mb-0">Αθηναϊκό</h6>
                                </div>
                                <div class="col-3">
                                    <h6 class="mb-0">Αθ/Θεσ</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <!-- Ενεργό -->
                                    <x-input-checkbox
                                        name="active"
                                        label=""
                                        value="{{ old('active') ?: $club->active }}"
                                    />
                                </div>
                                <div class="col-3">
                                    <!-- Πλήρες Μέλος -->
                                    <x-input-checkbox
                                        name="normal_member"
                                        label=""
                                        value="{{ old('active') ?: $club->normal_member }}"
                                    />
                                </div>
                                <div class="col-3">
                                    <!-- Αθηναϊκό -->
                                    <x-input-checkbox
                                        name="in_athens"
                                        label=""
                                        value="{{ old('active') ?: $club->in_athens }}"
                                    />
                                </div>
                                <div class="col-3">
                                    <!-- Αθήνας/Θεσσαλονίκης -->
                                    <x-input-checkbox
                                        name="in_major_city"
                                        label=""
                                        value="{{ old('active') ?: $club->in_major_city }}"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Επικοινωνία -->
            <div class="col">
                <div class="card">
                    <h4 class="bg-gray-300 text-secondary rounded-top p-3">Επικοινωνία</h4>
                    <div class="card-body">
                        <div class="card-text">
                            <!-- Υπεύθυνος -->
                            @if ($club->manager)
                            <div class="row mb-4">
                                <div class="col-1 text-center">
                                    <span class="align-bottom d-block"><i class="fas fa-user text-cyan"></i></span>
                                    <span class="align-bottom d-block mt-2"><i class="fas fa-phone"></i></span>
                                    <span class="align-bottom d-block mt-2"><i class="fas fa-mobile"></i></span>
                                    <span class="align-bottom d-block mt-2"><i class="fas fa-envelope"></i></span>
                                </div>
                                <div class="col-9">
                                    <span class="align-bottom d-block"><a href="{{ route('people.show', $club->manager) }}">{{ $club->manager->full_name }}</a>&nbsp;</span>
                                    <span class="align-bottom d-block mt-2"><a href="tel:{{ $club->manager->phone }}">{{ $club->manager->phone }}</a>&nbsp;</span>
                                    <span class="align-bottom d-block mt-2"><a href="tel:{{ $club->manager->mobile }}">{{ $club->manager->mobile }}</a>&nbsp;</span>
                                    <span class="align-bottom d-block mt-2">
                                        <a href="mailto:{{ $club->manager->full_name }}<{{ $club->manager->email }}>" target="_blank">{{ $club->manager->email }}</a>
                                        @if($club->manager->email)<i class="text-sm fas fa-external-link-alt text-gray-400"></i>@endif
                                    </span>
                                </div>
                                <div class="col-2">
                                    <button
                                        id="managerModalButton"
                                        type="button"
                                        class="disabled btn btn-secondary px-2 me-1"
                                        data-bs-toggle="modal"
                                        data-bs-target="#managerModal"
                                    >
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            @else
                            <div class="row mb-4">
                                <div class="col-1 text-center"></div>
                                <div class="col-9">Υπεύθυνος</div>
                                <div class="col-2">
                                    <button
                                        id="managerModalButton"
                                        type="button"
                                        class="disabled btn btn-secondary px-2 me-1"
                                        data-bs-toggle="modal"
                                        data-bs-target="#managerModal"
                                    >
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            @endif
                            <!-- Διευθύνσεις -->
                            @if ($club->addresses->count() > 0)
                                @if ($club->headquarters())
                                    <div class="row mb-4">
                                        <div class="col-1 text-center">
                                            <i class="fas fa-map-marker text-cyan"></i>
                                        </div>
                                        <div class="col-9">
                                            {!! nl2br($club->headquarters()->toMultiline()) !!}
                                        </div>
                                        <div class="col-2">
                                            <button
                                                id="addressesModalButton"
                                                type="button"
                                                class="btn btn-secondary px-2 me-1"
                                                data-bs-toggle="modal"
                                                data-bs-target="#addressesModal"
                                            >
                                                <i class="fas fa-align-justify"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                                @foreach ($club->nonHeadquarters() as $item)
                                    <div class="row mb-4">
                                        <div class="col-1 text-center">
                                            <i class="fas fa-map-marker"></i>
                                        </div>
                                        <div class="col-9">
                                            {!! nl2br($item->toMultiline()) !!}
                                        </div>
                                        <div class="col-2">
                                            @once
                                            @if (!$club->headquarters() && $item)
                                            <button
                                                id="addressesModalButton"
                                                type="button"
                                                class="btn btn-secondary px-2 me-1"
                                                data-bs-toggle="modal"
                                                data-bs-target="#addressesModal"
                                            >
                                                <i class="fas fa-align-justify"></i>
                                            </button>
                                            @endif
                                            @endonce
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="row mb-4">
                                    <div class="col-1 text-center"></div>
                                    <div class="col-9">Διευθύνσεις</div>
                                    <div class="col-2">
                                        <button
                                            id="addressesModalButton"
                                            type="button"
                                            class="btn btn-secondary px-2 me-1"
                                            data-bs-toggle="modal"
                                            data-bs-target="#addressesModal"
                                        >
                                            <i class="fas fa-align-justify"></i>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <x-addresses-modal :club="$club" />
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cols-2 g-2 mb-4">
            <!-- EOM -->
            <div class="col">
                <div class="card">
                    <h4 class="bg-gray-300 text-secondary rounded-top p-3">Στοιχεία ΕΟΜ</h4>
                    <div class="card-body">
                        <div class="card-text">
                            <!-- Ημερομηνία Ίδρυσης -->
                            <x-input
                                name="founded"
                                type="text"
                                label="Ημερομηνία Ίδρυσης"
                                value="{{ old('founded') ?: ($club->founded ? $club->founded->format('d/m/Y') : '') }}"
                            />
                            <!-- Καταστατικό -->
                            <x-input
                                name="bylaws"
                                type="text"
                                label="Καταστατικό"
                                value="{{ old('bylaws') ?: $club->bylaws }}"
                            />
                            <!-- Ημερομηνία Εγγραφής -->
                            <x-input
                                name="subscription_date"
                                type="text"
                                label="Ημερομηνία Εγγραφής"
                                value="{{ old('subscription_date') ?: ($club->subscription_date ? $club->subscription_date->format('d/m/Y') : '') }}"
                            />
                            <!-- Πρωτόκολλο Εγγραφής -->
                            <x-input
                                name="subscription_protocol"
                                type="text"
                                label="Πρωτόκολλο"
                                value="{{ old('subscription_protocol') ?: $club->subscription_protocol }}"
                            />
                            <!-- Ημερομηνία Μέλους -->
                            <x-input
                                name="membership_date"
                                type="text"
                                label="Ημερομηνία Μέλους"
                                value="{{ old('membership_date') ?: ($club->membership_date ? $club->membership_date->format('d/m/Y') : '') }}"
                            />
                            <!-- Πρωτόκολλο Μέλους -->
                            <x-input
                                name="membership_protocol"
                                type="text"
                                label="Πρωτόκολλο"
                                value="{{ old('membership_protocol') ?: $club->membership_protocol }}"
                            />
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <!-- Εφορία -->
                <div class="card">
                    <h4 class="bg-gray-300 text-secondary rounded-top p-3">Στοιχεία Εφορίας</h4>
                    <div class="card-body">
                        <div class="card-text">
                            <!-- ΑΦΜ -->
                            <x-input
                                name="irs_vat_number"
                                type="text"
                                label="ΑΦΜ"
                                value="{{ old('irs_vat_number') ?: $club->irs_vat_number }}"
                            />
                            <!-- ΔΟΥ -->
                            <x-input
                                name="irs_location"
                                type="text"
                                label="ΔΟΥ  "
                                value="{{ old('irs_location') ?: $club->irs_location }}"
                            />
                            <!-- Έδρα -->
                            <x-input
                                name="irs_headquarters"
                                type="text"
                                label="Έδρα"
                                value="{{ old('irs_headquarters') ?: $club->irs_headquarters }}"
                            />
                        </div>
                    </div>
                </div>

                <!-- ΓΓΑ -->
                <div class="card">
                    <h4 class="bg-gray-300 text-secondary rounded-top p-3">Στοιχεία ΓΓΑ</h4>
                    <div class="card-body">
                        <div class="card-text">
                            <!-- Κωδικός Μητρώου ΓΓΑ -->
                            <x-input
                                name="gga_code"
                                type="text"
                                label="Κωδικός"
                                value="{{ old('gga_code') ?: $club->gga_code }}"
                            />
                            <!-- Πρωτόκολλο Εγγραφής -->
                            <x-input
                                name="gga_protocol"
                                type="text"
                                label="Πρωτόκολλο"
                                value="{{ old('gga_protocol') ?: $club->gga_protocol }}"
                            />
                            <!-- Αναγνωρισμένο -->
                            <input type="hidden" name="gga_recognized" value="0" />
                            <x-input-checkbox
                                name="gga_recognized"
                                label="Αναγνωρισμένο"
                                tabindex="-1"
                                value="{{ old('gga_recognized') ?: $club->gga_recognized }}"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-print-none d-flex align-items-stretch justify-content-between">
            <x-button class="btn-primary">{{ $club->id ? 'Ενημέρωση' : 'Αποθήκευση' }}</x-button>
        </div>
    </form>

    <x-slot name="toolbar">
        <div class="btn-group">
            <x-tool href="{{ $club->id ? route('clubs.show', ['club' => $club]) : route('clubs.index') }}"
                icon="fas fa-angle-left" />
        </div>
    </x-slot>
</x-app-layout>
