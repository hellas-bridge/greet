<x-app-layout>
    <x-slot name="title">Σωματεία</x-slot>

    <x-slot name="header">Κατάλογος σωματείων</x-slot>

    <div class="card">
        <table class="table table-hover table-striped my-0 d-none">
            <thead>
                <tr>
                    <th>Όνομα</th>
                    <th>Email</th>
                    <th>Ενεργό</th>
                    <th>Πλήρες μέλος</th>
                    <th>Αθηναϊκό</th>
                    <th>Αθ/Θεσ</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clubs as $club)
                <tr data-url="{{ route('clubs.show', ['club' => $club]) }}" class="clicky pointy{{ $club->trashed() ? ' bg-warning' : '' }}">
                    <td{!! $club->trashed() ? ' class="text-decoration-line-through"' : '' !!}>{{ $club->name }}</td>
                    <td{!! $club->trashed() ? ' class="text-decoration-line-through"' : '' !!}>{{ $club->email }}</td>
                    <td><x-check-icon value="{{ $club->active }}" /></td>
                    <td><x-check-icon value="{{ $club->normal_member }}" /></td>
                    <td><x-check-icon value="{{ $club->in_athens }}" /></td>
                    <td><x-check-icon value="{{ $club->in_major_city }}" /></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <x-slot name="toolbar">
        <div class="d-flex align-items-stretch justify-content-between">
            <x-tool href="{{ route('clubs.index') }}{{ $trashed ? '' : '?trashed' }}"
                icon="{{ $trashed ? 'far fa-trash-alt' : false }}"
                type="{{ $trashed ? 'btn-warning' : false }}">
                {{ $trashed ? 'όλα' : 'ενεργά' }}
            </x-tool>
            <x-tool href="{{ route('clubs.create') }}"
                icon="far fa-file"
                type="btn-primary">
                προσθήκη
            </x-tool>
        </div>
    </x-slot>

    <x-slot name="extra_scripts">
        <script src="{{ mix('js/components/datatables.js') }}" defer></script>
        <script src="{{ mix('js/helpers/clubs.index.js') }}" defer></script>
    </x-slot>

</x-app-layout>
