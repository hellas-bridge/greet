<x-app-layout>
    <x-slot name="title">Αρχική</x-slot>

    <x-slot name="header">
        Κύρια σελίδα
    </x-slot>

    <div class="row">
        <div class="card py-4">
            {{ Markdown::parse(File::get('new-features.md')) }}
        </div>
    </div>

</x-app-layout>
