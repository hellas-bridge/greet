<!-- Main Nav -->
<nav class="navbar navbar-expand navbar-light navbar-bg">
    <a class="d-print-none sidebar-toggle js-sidebar-toggle">
        <i class="hamburger align-self-center"></i>
    </a>
    @isset($header)
        <!-- Header -->
        <h3 class="font-semibold text-gray-800 leading-tight mb-0 pb-0">
            {{ $header }}
        </h3>
    @endisset

    <div class="navbar-collapse collapse">
        <div class="d-none d-print-flex navbar-nav navbar-align sidebar-brand">
            <x-application-logo
                class="logo-small p-0 m-0 align-middle d-inline-block"
                text="REET" />
        </div>
        <ul class="d-print-none navbar-nav navbar-align">
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
                    <i class="fas fa-align-left"></i>
                </a>

                <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
                    <span class="text-dark">{{ Auth::user()->name }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- Settings -->
                    <a class="dropdown-item" href="{{ route('users.show', ['user' => Auth::user()]) }}">
                        <i class="fas fa-user"></i>
                        <span class="align-middle">Προφίλ</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <!-- Authentication -->
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf

                        <a class="dropdown-item" href="route('logout')"
                            onclick="event.preventDefault();
                            this.closest('form').submit();">
                            Αποσύνδεση
                        </a>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>
