<footer class="footer d-print-none">
    <div class="container-fluid">
        <div class="row row-cols-2 text-muted">
            <div class="col text-start">
                <p class="mb-0 text-muted">
                    Ελληνική Ομοσπονδία Μπριτζ © 2021
                </p>
            </div>
            {{-- <div class="col text-end">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a class="p-0 m-0 btn disabled" href="#" tabindex="-1" role="button" aria-disabled="true">Υποστήριξη</a>
                    </li>
                </ul>
            </div> --}}
        </div>
    </div>
</footer>
