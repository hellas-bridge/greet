<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Takis Pournaras">
    <link rel="shortcut icon" href="/logo.svg">

    <title>{{ isset($title) ? $title . ' | ' : '' }}GREET</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    @isset($extra_scripts){{ $extra_scripts }}@endisset

</head>
<body>
    <div class="wrapper">
        <!-- Navigation -->
        @include('layouts.sidebar')

        <!-- Main -->
        <div class="main">
            @include('layouts.header')

            @include('layouts.toolbar')

            <!-- Page Content -->
            <main class="content">
                <div class="container-fluid p-0">
                    {{ $slot }}
                </div>
            </main>

            <!-- Page Footer -->
            @include('layouts.footer')
        </div>
    </div>
    @include('flash-messages')
</body>
</html>
