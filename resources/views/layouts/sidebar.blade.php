<nav id="sidebar" class="d-print-none sidebar js-sidebar {{ $cookies['sidebar'] ?? '' }}">
    <div class="sidebar-content js-simplebar" data-simplebar="init">
        <div class="simplebar-wrapper" style="margin: 0px;">
            <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
            </div>
            <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                    <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden scroll;">
                        <div class="simplebar-content">
                            <a class="sidebar-brand" href="{{ route('home') }}">
                                <x-application-logo
                                    class="logo-small p-0 m-0 align-middle d-inline-block"
                                    text="REET" />
                            </a>

                            <ul class="sidebar-nav">
                                <li class="sidebar-header">Σελίδες</li>

                                <!-- home -->
                                <li class="sidebar-item{{ activeLink('/') }}">
                                    <a class="sidebar-link" href="/">
                                        <i class="fas fa-home"></i>
                                        <span class="align-middle">Αρχική</span>
                                    </a>
                                </li>

                                <!-- users -->
                                @can('viewAny', App\Models\User::class)
                                <li class="sidebar-item{{ activeLink('users') }}">
                                    <a class="sidebar-link" href="{{ route('users.index') }}">
                                        <i class="fas fa-users"></i>
                                        <span class="align-middle">Χρήστες</span>
                                    </a>
                                </li>
                                @endcan

                                <!-- clubs -->
                                @can('viewAny', App\Models\Club::class)
                                <li class="sidebar-item{{ activeLink('clubs') }}">
                                    <a class="sidebar-link" href="{{ route('clubs.index') }}">
                                        <i class="fas fa-building"></i>
                                        <span class="align-middle">Σωματεία</span>
                                    </a>
                                </li>
                                @endcan

                                <!-- contacts -->
                                @can('viewAny', App\Models\Person::class)
                                <li class="sidebar-item{{ activeLink('people') }}">
                                    <a class="sidebar-link" href="{{ route('people.index') }}">
                                        <i class="fas fa-id-card"></i>
                                        <span class="align-middle">Επαφές</span>
                                    </a>
                                </li>
                                @endcan

                                <!-- abilities -->
                                @can('viewAny', App\Models\Ability::class)
                                <li class="sidebar-item{{ activeLink('abilities') }}">
                                    <a class="sidebar-link" href="{{ route('abilities.index') }}">
                                        <i class="fas fa-list-ul"></i>
                                        <span class="align-middle">Ιδιότητες</span>
                                    </a>
                                </li>
                                @endcan
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="simplebar-placeholder" style="width: auto; height: 957px;"></div>
        </div>
        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
            <div class="simplebar-scrollbar" style="width: 0px; display: none;"></div>
        </div>
        <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
            <div class="simplebar-scrollbar" style="height: 509px; display: block; transform: translate3d(0px, 0px, 0px);"></div>
        </div>
    </div>
</nav>
