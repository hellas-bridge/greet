<!-- table navigation -->
<div class="row align-items-center mt-4">
    <div class="col-4 text-muted">
        @if ($data->total() == 0)
            Δε βρέθηκαν εγγραφές
        @else
            {{ $data->firstItem() }} έως {{ $data->lastItem() }} από {{ $data->total() }} στοιχεία
        @endif
    </div>
    <div class="col-8 d-print-none">
        <div class="dataTables_paginate">
            {{ $data->links() }}
        </div>
    </div>
</div>
