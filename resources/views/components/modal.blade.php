@props(['static' => false, 'id' => '', 'title' => '', 'centered' => false, 'xButton' => false, 'cancelButton' => 'true'])

<div class="modal fade" id="{{ $id }}" {{ $static ? 'data-bs-backdrop="static" ' : ''}}data-bs-keyboard="{{ $xButton ? 'true' : 'false' }}" tabindex="-1" aria-labelledby="{{ $id }}Label" aria-hidden="true">
    <div class="modal-dialog modal-lg{{ $centered ? ' modal-dialog-centered' : '' }}">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="{{ $id }}Label">{{ $title }}</h4>
                @if ($xButton)
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                @endif
            </div>
            <div class="modal-body">
                {{ $slot }}
            </div>
            <div class="modal-footer">
                @if ($cancelButton == 'true')
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Άκυρο</button>
                @endif
                <button type="button" class="btn btn-primary" id="{{ $id }}OkButton"@if ($cancelButton == 'false') data-bs-dismiss="modal"@endif>Εντάξει</button>
            </div>
        </div>
    </div>
</div>
