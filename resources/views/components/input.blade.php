@props([
    'name',
    'label',
    'disabled' => false,
    'required' => false,
    'autofocus' => false,
    'labelClass' => false,
    'class' => '',
    'small' => false,
    'divclass' => '',
])

@php
    $class = trim($class . ' form-control');
    $divclass = ($divclass ?: '') . ' form-floating mb-3';
@endphp

@error($name)
    @php
        $class .= ' is-invalid';
    @endphp
@enderror

<div class="{{ $divclass }}">
    <input {!! $attributes->merge(['id' => $name, 'name' => $name, 'placeholder' => $attributes['type'] == 'date' ? 'dd-mm-yyyy' : $name, 'type' => 'text', 'class' => $class]) !!}{{ $disabled ? ' disabled' : '' }}{{ $required ? ' required' : '' }}{{ $autofocus ? ' autofocus' : '' }} />
    <label class="{{ $labelClass ?: 'text-black-50' }}" for="{{ $name }}">{{ $label }}</label>
    @if ($small)
    <div class="text-end"><small>{!! $small !!}</small></div>
    @endif
</div>

@if($autofocus)
<script>
    document.addEventListener("DOMContentLoaded", () => {
        document.querySelector('input[autofocus]').select();
    });
</script>
@endif
