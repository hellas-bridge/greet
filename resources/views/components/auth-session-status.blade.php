@props(['status'])

@if ($status)
    <p {{ $attributes->merge(['class' => 'text-success mb-4']) }}>
        {{ $status }}
    </p>
@endif
