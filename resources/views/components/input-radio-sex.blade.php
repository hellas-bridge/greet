@props([
    'value' => false,
])

<div class="mb-4">
    Φύλλο
    <div class="form-check form-check-inline">
        <input class="form-check-input"
            type="radio"
            name="male"
            id="inlineRadioSex1"
            value="1"
            @if ($value == 1)
            checked
            @endif
            />
        <label class="form-check-label" for="inlineRadioSex1">άνδρας</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input"
            type="radio"
            name="male"
            id="inlineRadioSex2"
            value="0"
            @if ($value == 0)
            checked
            @endif
            />
        <label class="form-check-label" for="inlineRadioSex2">γυναίκα</label>
    </div>
</div>
