@props([
    'name',
    'label',
    'disabled' => false,
    'checked' => false,
    'value' => false,
    'labelClass' => false
])

<div class="form-check mb-4">
    <input {!! $attributes->merge([
        'id' => $name,
        'name' => $name,
        'type' => 'checkbox',
        'class' => 'form-check-input'
    ]) !!}{{ $disabled ? ' disabled' : '' }}{{ $checked || $value ? ' checked' : '' }} />
    <label class="{{ $labelClass ?: 'text-black-50' }}" for="{{ $name }}">{{ $label }}</label>
</div>
