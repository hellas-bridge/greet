<x-modal id="addressesModal" static="true" title="new address" cancelButton="false">
    <x-slot name="title">Διευθύνσεις</x-slot>
    <table id="addressesTable" style="width:100%;">
        <thead>
            <tr>
                <th>Έδρα</th>
                <th>Οδός</th>
                <th>Περιοχή</th>
                <th>ΤΚ</th>
                <th>Πόλη</th>
                <th><abbr title="Αφήστε το κενό για 'Ελλάδα'">Χώρα</abbr></th>
                <th>Τηλέφωνο</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($club->addresses as $address)
                <tr>
                    <td>
                        <input type="hidden" name="address.id[]" value="{{ $address->id }}" />
                        <input type="hidden" name="address.deleted[]" value="false" />
                        <input type="radio" name="address.main" class="form-check-input" {{ $address->main ? 'checked ' : '' }} value="row-{{ $loop->index }}" />
                    </td>
                    <td>
                        <input type="text" name="address.street[]" value="{{ $address->street }}" class="form-control form-control-sm" />
                    </td>
                    <td>
                        <input type="text" name="address.region[]" value="{{ $address->region }}" class="form-control form-control-sm" />
                    </td>
                    <td>
                        <input type="text" name="address.zip[]" value="{{ $address->zip }}" class="form-control form-control-sm" />
                    </td>
                    <td>
                        <input type="text" name="address.city[]" value="{{ $address->city }}" class="form-control form-control-sm" />
                    </td>
                    <td>
                        <input type="text" name="address.country[]" value="{{ $address->country }}" class="form-control form-control-sm" />
                    </td>
                    <td>
                        <input type="text" name="address.phone[]" value="{{ $address->phone }}" class="form-control form-control-sm" />
                    </td>
                    <td>
                        <button
                            type="button"
                            class="btn btn-warning px-2 me-1"
                            onclick="deleteRow(this)"
                        >
                             text-center
                        </button>
                        <button
                            type="button"
                            class="btn btn-success px-2 me-1 d-none"
                            onclick="undeleteRow(this)"
                        >
                            <i class="fas fa-plus"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7"></td>
                <td>
                    <button
                        id="addLineToAddress"
                        type="button"
                        class="btn btn-secondary px-2 me-1"
                    >
                        <i class="fas fa-file"></i>
                    </button>
                </td>
            </tr>
        </tfoot>
    </table>
</x-modal>
<script>
    document.getElementById('addLineToAddress')
        .addEventListener('click', function (event) {
            event.preventDefault()
            var body = document.getElementById('addressesTable')
                .getElementsByTagName('tbody')[0]
            body.insertRow().innerHTML =
                '<td><input type="hidden" name="address.deleted[]" value="false" />' +
                '<input type="hidden" name="address.id[]" value="new" />' +
                '<input type="radio" name="address.main" class="form-check-input" value="row-' + body.rows.length + '" /></td>' +
                '<td><input type="text" name="address.street[]" value="" class="form-control form-control-sm" /></td>' +
                '<td><input type="text" name="address.region[]" value="" class="form-control form-control-sm" /></td>' +
                '<td><input type="text" name="address.zip[]" value="" class="form-control form-control-sm" /></td>' +
                '<td><input type="text" name="address.city[]" value="" class="form-control form-control-sm" /></td>' +
                '<td><input type="text" name="address.country[]" value="" class="form-control form-control-sm" /></td>' +
                '<td><input type="text" name="address.phone[]" value="" class="form-control form-control-sm" /></td>' +
                '<td><button type="button" class="btn btn-warning px-2 me-1" onclick="deleteRow(this)"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-success px-2 me-1 d-none" onclick="undeleteRow(this)"><i class="fas fa-plus"></i></button></td>'
            body.rows[body.rows.length -1].cells[1]
                .getElementsByTagName('input')[0].focus()
        }
    );

    function deleteRow(el)
    {
        // get the row
        var tr = el.parentNode.parentNode
        // set hidden deleted to true
        tr.querySelectorAll('input[name="address.deleted[]"]')[0].value = "true"
        // disable all inputs
        tr.getElementsByTagName('input').forEach(element => {
            element.readOnly = true
        });
        // hide this button
        el.classList.add('d-none')
        // reveal undelete button
        el.nextElementSibling.classList.remove('d-none')
        return false;
    }

    function undeleteRow(el)
    {
        // get the row
        var tr = el.parentNode.parentNode
        // set hidden deleted to false
        tr.querySelectorAll('input[name="address.deleted[]"]')[0].value = "false"
        // enable all inputs
        tr.getElementsByTagName('input').forEach(element => {
            element.readOnly = false
        });
        // hide this button
        el.classList.add('d-none')
        // reveal delete button
        el.previousElementSibling.classList.remove('d-none')
        return false;
    }
</script>