@props([
    'name',
    'label',
    'value' => '',
])

<div class="image-upload">
    <div class="image-preview"
        @if ($value !== '')
        style="background-image: url({{ $value }})"
        @endif
        data-background="{{ $value }}"></div>
    <label class="btn btn-primary btn-upload">
        Φόρτωση
        <input type="file"
            class="upload-file-hidden"
            name="{{ $name }}"
            id="{{ $name }}"
            accept="image/*"
            data-value="{{ $value }}" />
    </label>
    <input type="hidden" name="{{ $name}}_delete" value="0" id="image-hidden-field" />
    <div class="image-remove-icon-minus d-none"><i class="fas fa-minus"></i></div>
    <div class="image-remove-icon-delete{{ $value == '' ? ' d-none' : '' }}"><i class="fas fa-trash-alt text-white"></i></div>
</div>

<x-slot name="extra_scripts"><script src="{{ mix('js/components/input-file-upload.js') }}" defer></script></x-slot>
