@props(['href' => false, 'icon' => false, 'type' => 'btn-tool-white'])

@if ($href)
<a href="{{ $href }}"
@else
<button
@endif
    {{ $attributes->merge(['class' => "btn $type btn-tool fs-5 shadow-sm"]) }}>
    @if ($icon !== false)
        <div class="tool-icon">
            <i class="{{ $icon }}"></i>
        </div>
    @endif
    @if ((string)$slot !== '')
        <div class="tool-text{{ !$icon ? '': ' ms-1' }}">
            {{ $slot }}
        </div>
    @endif
@if ($href)
</a>
@else
</button>
@endif
