@props(['href' => false, 'icon' => false, 'fill', 'stroke'])

@if ($href !== false)
    <a href="{{ $href }}" {{ $attributes->merge(['class' => 'btn btn-primary' . ($icon ? ' position-relative d-block' : '')]) }}>
@else
    <button {{ $attributes->merge(['type' => 'submit', 'class' => 'btn btn-primary' . ($icon ? ' position-relative d-block' : '')]) }}>
@endif
        @if($icon)
            <span class="align-middle position-absolute d-inline-block btn-label">
                <i class="fas fa-{{$icon}}{{ isset($stroke) ? 'text-' . $stroke : '' }}"></i>
            </span>
        @endif
        <span @if($icon)class="btn-label-text text-centered"@endif>{{ $slot }}</span>
@if ($href !== false)
    </a>
@else
    </button>
@endif
