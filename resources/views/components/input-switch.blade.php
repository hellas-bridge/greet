@props([
    'name',
    'label',
    'value' => false,
    'disabled' => false,
    'labelClass' => false
])
<div class="form-check form-switch">
    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
    <input {!! $attributes->merge([
        'id' => $name,
        'name' => $name,
        'type' => 'checkbox',
        'class' => 'form-check-input'
    ]) !!}{{ $disabled ? ' disabled' : '' }}{{ $value ? ' checked': '' }} data-on="1" data-off="0" />
    <label class="{{ $labelClass ?: 'form-check-label' }}" for="{{ $name }}">{{ $label }}</label>
</div>