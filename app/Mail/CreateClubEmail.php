<?php

namespace App\Mail;

use App\Models\Club;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreateClubEmail extends Mailable
{
    use Queueable, SerializesModels;

    public ?Club $club;

    public function __construct(Club $club)
    {
        $this->club = $club;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Δημιουργία λογαριασμού email για σωματείο')
            ->markdown('emails.admin.create-club-email');
    }
}
