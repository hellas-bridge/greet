<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Person;
use Illuminate\Auth\Access\HandlesAuthorization;

class PersonPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Person $model): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Person $model): bool
    {
        return true;
    }

    public function delete(User $user, Person $model): bool
    {
        return true;
    }

    public function restore(User $user, Person $model): bool
    {
        return true;
    }

    public function forceDelete(User $user, Person $model): bool
    {
        return false;
    }
}
