<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Ability;
use Illuminate\Auth\Access\HandlesAuthorization;

class AbilityPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Ability $model): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Ability $model): bool
    {
        return true;
    }

    public function delete(User $user, Ability $model): bool
    {
        return true;
    }

    public function restore(User $user, Ability $model): bool
    {
        return true;
    }

    public function forceDelete(User $user, Ability $model): bool
    {
        return false;
    }
}
