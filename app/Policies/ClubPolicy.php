<?php

namespace App\Policies;

use App\Models\Club;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClubPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Club $model): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Club $model): bool
    {
        return true;
    }

    public function delete(User $user, Club $model): bool
    {
        return true;
    }

    public function restore(User $user, Club $model): bool
    {
        return true;
    }

    public function forceDelete(User $user, Club $model): bool
    {
        return false;
    }
}
