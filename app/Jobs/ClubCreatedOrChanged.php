<?php

namespace App\Jobs;

use App\Mail\CreateClubEmail;
use App\Models\Club;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ClubCreatedOrChanged // implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public ?Club $club;

    public function __construct(Club $club)
    {
        $this->club = $club;
    }

    public function handle()
    {
        Mail::to(config('app.it-email'))->send(new CreateClubEmail($this->club));
        Log::channel('monitor')->info("Emailed club email creation for club #{$this->club->id} ({$this->club->name})");
    }
}
