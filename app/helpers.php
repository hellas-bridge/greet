<?php

if (!function_exists('activeLink')) {
    function activeLink(string $url) : string
    {
        // return request()->is($url) ? ' active' : '';
        return strpos(request()->path(), $url) === 0 ? ' active' : '';
    }
}

if (!function_exists('image_directory_chunk')) {
    function image_directory_chunk(?int $chunk) : string
    {
        $chunk = $chunk ?: 0;
        $dir = (int) ($chunk / 1000);
        return (string) ($dir + 1) * 1000;
    }
}

if (!function_exists('getDBDateFromInput')) {
    function getDBDateFromInput(?string $value): ?string
    {
        if (!is_null($value)) {
            $value = Illuminate\Support\Carbon::createFromFormat('d/m/Y', $value)
                ->format('Y-m-d');
        }
        return $value;
    }
}

if (!function_exists('numberFormat')) {
    function numberFormat(?float $number, int $decimals = 0): string
    {
        return number_format($number, $decimals, ',', '.');
    }
}

if (!function_exists('codeFormat')) {
    function codeFormat(?int $number): string
    {
        return str_pad($number, 5, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('categoryName')) {
    function categoryName(?int $category): string
    {
        switch ($category) {
            case '1':
                return 'Κατηγορία Δ';
            case '2':
                return 'Κατηγορία Γ';
            case '3':
                return 'Κατηγορία Β';
            case '4':
                return 'Κατηγορία Α';
            case '5':
                return 'Έμπειρος Δ';
            case '6':
                return 'Έμπειρος Γ';
            case '7':
                return 'Έμπειρος Β';
            case '8':
                return 'Έμπειρος Α';
            case '9':
                return 'Δόκιμος Μετρ';
            case '10':
                return 'Δόκιμος Μετρ';
            case '11':
                return 'Περιφερειακός Μετρ';
            case '12':
                return 'Μετρ';
            case '13':
                return 'Εθνικός Μετρ';
            case '14':
                return 'Ισόβιος Μετρ';
            case '15':
                return 'Μεγάλος Μετρ';
            case '16':
                return 'Ανώτατος Μετρ';
        }

        return '';
    }
}
