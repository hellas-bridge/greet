<?php

namespace App\Http\Controllers;

use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct()
    {
        // Use UserPolicy
        $this->authorizeResource(User::class);

        // Ask password during password update
        $this->middleware('password.confirm')->only(['edit', 'update']);
    }

    public function index(): View
    {
        return view('users.index')
            ->with('users', User::all());
    }

    public function create(): View
    {
        throw new \Exception('TODO');
    }

    public function store(Request $request): View
    {
        throw new \Exception('TODO');
    }

    public function show(User $user): View
    {
        return view('users.show')
            ->with('user', $user);
    }

    public function edit(Request $request, User $user): View
    {
        return view('users.edit')
            ->with('user', $user);
    }

    public function update(Request $request, User $user): RedirectResponse
    {
        $rules = [
            'name' => 'required|string|max:255',
            'username' => [
                'required',
                'string',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
        ];

        if (!is_null($request->password)) {
            $rules['password'] = 'required|string|confirmed|min:8';
        }

        $fields = $request->validate($rules);

        if (isset($fields['password'])) {
            $fields['password'] = Hash::make($fields['password']);
        }

        $user->update($fields);
        $request->session()->regenerate();

        return redirect(route('users.show', ['user' => $user]))
            ->with('info', 'Τα στοιχεία του χρήστη ενημερώθηκαν.');
    }

    public function destroy(User $user): View
    {
        throw new \Exception('TODO');
    }
}
