<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonPostRequest;
use App\Models\Person;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class PersonController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Person::class);
    }

    public function index(Request $request): View|JsonResponse
    {
        return view('people.index')
            ->with('trashed', $request->query->has('trashed'));
    }

    public function show(Person $person): View
    {
        return view('people.show')->with('person', $person);
    }

    public function create()
    {
        return view('people.edit')
            ->with('person', Person::make());
    }

    public function store(PersonPostRequest $request)//: RedirectResponse
    {

        $person = Person::create($this->get_merged_person_data($request));

        if ($request->image) {
            $this->savePersonImage($request->image, $person->imageFilename);
        }

        return redirect(route('people.show', ['person' => $person]))
            ->with('info', 'Τα στοιχεία της επαφής ενημερώθηκαν.');
    }

    public function edit(Person $person): View
    {
        return view('people.edit')
            ->with('person', $person);
    }

    public function update(PersonPostRequest $request, Person $person): RedirectResponse
    {
        $person->update($this->get_merged_person_data($request));

        if ($request->image || $request->image_delete) {
            Storage::delete($person->imageFilename);
        }

        if ($request->image) {
            $this->savePersonImage($request->image, $person->imageFilename);
        }

        return redirect(route('people.show', ['person' => $person]))
            ->with('info', 'Τα στοιχεία της επαφής ενημερώθηκαν.');
    }

    public function destroy(Person $person) : RedirectResponse
    {
        $person->delete();

        return redirect(route('people.index'))
            ->with('warning', 'Η επαφή έχει διαγραφεί.');
    }

    private function savePersonImage(UploadedFile $image, string $filename)
    {
        $path = dirname($filename);
        if (!Storage::exists($path)) {
            Storage::makeDirectory($path);
        }

        Image::make($image)
            ->fit('500', '500')
            ->encode('webp', 90)
            ->save(Storage::path($filename));
    }

    private function get_merged_person_data(PersonPostRequest $request): array
    {
        return array_merge(
            $request->validated(),
            [
                'male' => $request->has('male') && in_array($request->male, ['1', 'on']),
            ]
        );
    }
}
