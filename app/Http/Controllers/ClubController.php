<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClubPostRequest;
use App\Jobs\ClubCreatedOrChanged;
use App\Models\Club;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClubController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Club::class);
    }

    public function index(Request $request): View
    {
        $clubs = Club::orderBy('name');

        if ($request->query->has('trashed')) {
            $clubs = $clubs->withTrashed();
        }

        return view('clubs.index')
            ->with('clubs', $clubs->get())
            ->with('trashed', $request->query->has('trashed'));
    }

    public function create() : View
    {
        return view('clubs.edit')
            ->with('club', Club::make());
    }

    public function store(ClubPostRequest $request): RedirectResponse
    {
        DB::beginTransaction();

        $club = Club::create($this->get_merged_club_data($request));

        if (!$this->save_club_addresses($club, $request)) {
            DB::rollBack();
            abort(500, 'Πρόβλημα με την αποθήκευση διευθύνσεων.');
        }

        DB::commit();

        ClubCreatedOrChanged::dispatch($club);

        return redirect(route('clubs.show', ['club' => $club]))
            ->with('info', 'Τα στοιχεία του σωματείου αποθηκεύτηκαν.');
    }

    public function show(Club $club): View
    {
        $club->load('addresses', 'manager');

        return view('clubs.show')
            ->with('club', $club);
    }

    public function edit(Club $club): View
    {
        $club->load('addresses', 'manager');

        return view('clubs.edit')
            ->with('club', $club);
    }

    public function update(ClubPostRequest $request, Club $club): RedirectResponse
    {
        $old_email = $club->email;

        DB::beginTransaction();

        $club->update($this->get_merged_club_data($request));

        if (!$this->save_club_addresses($club, $request)) {
            DB::rollBack();
            abort(500, 'Πρόβλημα με την αποθήκευση διευθύνσεων.');
        }

        DB::commit();

        if ($old_email != $club->email) {
            ClubCreatedOrChanged::dispatch($club);
        }

        return redirect(route('clubs.show', ['club' => $club]))
            ->with('info', 'Τα στοιχεία του σωματείου αποθηκεύτηκαν.');
    }

    public function destroy(Club $club): RedirectResponse
    {
        $club->delete();

        return redirect(route('clubs.index'))
            ->with('warning', 'Το σωματείο έχει διαγραφεί.');
    }

    private function get_merged_club_data(ClubPostRequest $request): array
    {
        return array_merge(
            $request->validated(),
            [
                'active' => $request->has('active') && in_array($request->active, ['1', 'on']),
                'normal_member' => $request->has('normal_member') && in_array($request->normal_member, ['1', 'on']),
                'in_athens' => $request->has('in_athens') && in_array($request->in_athens, ['1', 'on']),
                'in_major_city' => $request->has('in_major_city') && in_array($request->in_major_city, ['1', 'on']),
                'gga_recognized' => $request->has('gga_recognized') && in_array($request->gga_recognized, ['1', 'on']),
            ]
        );
    }

    private function save_club_addresses(Club $club, ClubPostRequest $request): bool
    {
        if ($request->has('address_id')) {
            for ($w = 0; $w < count($request->address_id); $w++) {
                $address_data = [
                    'main' => $request->address_main === 'row-' . $w ? true : false,
                    'street' => $request->address_street[$w],
                    'region' => $request->address_region[$w],
                    'zip' => $request->address_zip[$w],
                    'city' => $request->address_city[$w],
                    'country' => $request->address_country[$w],
                    'phone' => $request->address_phone[$w],
                ];

                if ($request->address_id[$w] === 'new') {
                    if ($request->address_deleted[$w] === 'false') {
                        // new address non deleted
                        if (!$club->addresses()->create($address_data)) {
                            return false;
                        }
                    }
                } else {
                    $address = $club->addresses->find($request->address_id[$w]);
                    if (is_null($address)) {
                        Log::channel('single')->error('Could not find Address.', [
                            'controller' => (new \ReflectionClass(get_called_class()))->getShortName(),
                            'address_id' => $request->address_id[$w],
                            'club_id' => $club->id,
                        ]);
                        return false;
                    }

                    if ($request->address_deleted[$w] === 'false') {
                        // existing address non deleted
                        if (!$address->update($address_data)) {
                            Log::channel('single')->error('Could not update Address.', [
                                'controller', (new \ReflectionClass(get_called_class()))->getShortName(),
                                'address_id', $address->id,
                                'club_id', $club->id,
                            ]);
                            return false;
                        }
                    } else {
                        if (!$address->delete()) {
                            Log::channel('single')->error('Could not delete Address.', [
                                'controller', (new \ReflectionClass(get_called_class()))->getShortName(),
                                'address_id', $address->id,
                                'club_id', $club->id,
                            ]);
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}
