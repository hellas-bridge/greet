<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PersonController extends Controller
{
    public function index(Request $request)
    {
        $query = Person::select([
            'people.*',
            'clubs.name as clubname',
        ])->leftJoin('clubs', 'people.id', '=', 'clubs.manager_id');

        if ($request->query->has('trashed')) {
            $query = $query->withTrashed();
        }

        return Datatables::of($query)
            ->addColumn('url', function($row) {
                return route('people.show', $row->id, false);
            })
            ->addColumn('trashed', function($row) {
                return $row->trashed();
            })
            ->rawColumns(['url'])
            ->rawColumns(['trashed'])
            ->toJson();
    }
}
