<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function person(string $query = ''): JsonResponse
    {
        $data = collect([]);
        if ($query !== '') {
            $data = \App\Models\Person::select('id', 'last_name', 'first_name')
                ->with('athlete')
                ->where(DB::raw("TRIM(CONCAT(first_name, ' ', last_name))"), 'like', "%{$query}%")
                ->orWhereHas('athlete', function ($q) use ($query) {
                    $q->where('code', 'like', "{$query}%");
                })
                ->take(5)
                ->get();

            $data->map(function ($data) {
                if (!$data->athlete) {
                    $data->code = '';
                } else {
                    $data->code = $data->athlete->code;
                }
                $data->name = trim($data->first_name . ' ' . $data->last_name);
            });

            $data = $data->sortBy('code')->transform(function ($item) {
                unset($item->athlete);
                unset($item->first_name);
                unset($item->last_name);
                return $item;
            });

        }
        return response()->json($data->values());
    }

    public function club(string $query = ''): JsonResponse
    {
        $data = [];
        if ($query !== '') {
            $query = str_replace('.', '', $query);
            $data = \App\Models\Club::select('id', 'name')
                ->where('name', 'like', "{$query}%")
                ->orderBy('name')
                ->take(5)
                ->get();
        }
        return response()->json($data);
    }
}
