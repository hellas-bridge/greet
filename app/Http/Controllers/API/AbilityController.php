<?php

namespace App\Http\Controllers\API;

use App\Models\Ability;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class AbilityController extends Controller
{
    public function index(Request $request)
    {
        $query = Ability::select([
            'abilities.*',
            DB::raw('COUNT(`ability_club_person`.`person_id`) as `people`'),
            DB::raw('COUNT(`ability_club_person`.`club_id`) as `clubs`'),
        ])->leftJoin('ability_club_person', 'abilities.id', '=', 'ability_club_person.ability_id');

        $query->groupBy('id');

        if ($request->query->has('trashed')) {
            $query = $query->withTrashed();
        }


        return Datatables::of($query)
            ->addColumn('url', fn ($row) => route('abilities.show', $row->id, false))
            ->addColumn('trashed', fn ($row) => $row->trashed())
            ->rawColumns(['url'])
            ->rawColumns(['trashed'])
            ->toJson();
    }

    public function show(Ability $ability)
    {
        $query = DB::table('ability_club_person')
            ->leftJoin('clubs', 'ability_club_person.club_id', 'clubs.id')
            ->leftJoin('people', 'ability_club_person.person_id', 'people.id')
            ->select('ability_club_person.*', 'clubs.name as club', 'people.last_name as person')
            ->where('ability_club_person.ability_id', '=', $ability->id);

        return Datatables::of($query)
            ->editColumn('starting_date', function ($row) {
                return $row->starting_date == '' ? '' :
                    Carbon::createFromFormat('Y-m-d', $row->starting_date)->format('d/m/Y');
            })
            ->editColumn('ending_date', function ($row) {
                return $row->ending_date == '' ? '' :
                    Carbon::createFromFormat('Y-m-d', $row->ending_date)->format('d/m/Y');
            })
            ->editColumn('active', fn ($row) => $row->active == 0 ? false : true)
            ->toJson();
    }
}
