<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\AbilityClubPersonRequest;
use App\Models\AbilityClubPerson as ModelsAbilityClubPerson;

class AbilityClubPersonController extends Controller
{
    public function store(AbilityClubPersonRequest $request)
    {
        $model = ModelsAbilityClubPerson::create($request->validated());

        $query = DB::table('ability_club_person')
            ->leftJoin('clubs', 'ability_club_person.club_id', 'clubs.id')
            ->leftJoin('people', 'ability_club_person.person_id', 'people.id')
            ->select('ability_club_person.*', 'clubs.name as club', 'people.last_name as person')
            ->where('ability_club_person.id', '=', $model->id);

        return Datatables::of($query)
            ->editColumn('starting_date', function ($row) {
                return $row->starting_date == '' ? '' :
                    Carbon::createFromFormat('Y-m-d', $row->starting_date)->format('d/m/Y');
            })
            ->editColumn('ending_date', function ($row) {
                return $row->ending_date == '' ? '' :
                    Carbon::createFromFormat('Y-m-d', $row->ending_date)->format('d/m/Y');
            })
            ->editColumn('active', fn ($row) => $row->active == 0 ? false : true)
            ->toJson();
        return response()->json([
            'result' => true,
            'data' => $model,
        ]);
    }

    public function switch(int $id)
    {
        $model = ModelsAbilityClubPerson::findOrFail($id);
        $model->active = !$model->active;
        $model->save();
        $result = $model->active;
        return response()->json(['result' => $result]);
    }

    public function delete(int $id)
    {
        $model = ModelsAbilityClubPerson::findOrFail($id);
        $model->delete();
        return response()->json(['result' => true]);
    }
}
