<?php

namespace App\Http\Controllers;

use App\Models\Ability;
use App\Models\Club;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class AbilityController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Ability::class);
    }

    public function index(Request $request): View|JsonResponse
    {
        return view('abilities.index')
            ->with('trashed', $request->query->has('trashed'));
    }

    public function show(Ability $ability, Request $request): View|JsonResponse
    {
        $clubs = Club::select(['id', 'name'])->orderBy('name')->get();

        return view('abilities.show')
            ->with([
                'ability' => $ability,
                'clubs' => $clubs,
            ]);
    }

    public function edit(Ability $ability)
    {
        return view('abilities.edit')
            ->with('ability', $ability);
    }

    public function destroy(Ability $ability)
    {
        $ability->delete();

        return redirect(route('abilities.index'))
            ->with('warning', 'Η ιδιότητα έχει διαγραφεί.');
    }
}
