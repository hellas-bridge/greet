<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AbilityClubPersonRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'ability_id' => 'required|int',
            'person_id' => 'nullable|int',
            'club_id' => 'nullable|int',
            'active' => 'boolean',
            'description' => 'nullable|string|max:255',
            'starting_date' => 'nullable|date_multi_format:"d/m/Y","j/m/Y","d/n/Y","j/n/Y"',
            'ending_date' => 'nullable|date_multi_format:"d/m/Y","j/m/Y","d/n/Y","j/n/Y"',
        ];
    }
}
