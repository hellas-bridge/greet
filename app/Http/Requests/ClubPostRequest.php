<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClubPostRequest extends FormRequest
{
    public function rules(): array
    {
        $uniqueRule = $this->club ?
            Rule::unique('clubs')->ignore($this->club->id) :
            Rule::unique('clubs');

        return [
            'name' => [
                'required',
                'string',
                'max:255',
                $uniqueRule,
            ],
            'full_name' => 'nullable|string|max:255',
            'email' => [
                'nullable',
                'email',
                'max:255',
                $uniqueRule,
            ],
            'founded' => 'nullable|string|max:255',
            'website' => [
                'nullable',
                'url',
                'max:255',
            ],
            'irs_vat_number' => 'nullable|digits:9',
            'irs_location' => 'nullable|string|max:255',
            'irs_headquarters' => 'nullable|string|max:255',
            'bylaws' => 'nullable|string|max:255',
            'subscription_date' => 'date_multi_format:"d/m/Y","j/m/Y","d/n/Y","j/n/Y"',
            'subscription_protocol' => 'nullable|string|max:255',
            'membership_date' => 'nullable|date_multi_format:"d/m/Y","j/m/Y","d/n/Y","j/n/Y"',
            'membership_protocol' => 'nullable|string|max:255',
            'gga_code' => 'nullable|string|max:255',
            'gga_protocol' => 'nullable|string|max:255',

            // Addresses
            'address_main' => 'string',
            'address_id' => 'array',
            'address_id.*' => 'string',
            'address_deleted' => 'array',
            'address_deleted.*' => 'in:true,false',
            'address_street' => 'array',
            'address_street.*' => 'nullable|string',
            'address_region' => 'array',
            'address_region.*' => 'nullable|string',
            'address_zip' => 'array',
            'address_zip.*' => 'nullable|string',
            'address_city' => 'array',
            'address_city.*' => 'nullable|string',
            'address_country' => 'array',
            'address_country.*' => 'nullable|string',
            'address_phone' => 'array',
            'address_phone.*' => 'nullable|string',
        ];
    }
}
