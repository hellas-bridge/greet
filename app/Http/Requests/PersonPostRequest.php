<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonPostRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'last_name' => 'required|string|max:255',
            'first_name' => 'nullable|string|max:255',
            'initial' => 'nullable|string|max:255',
            'email' => 'nullable|email|max:255',
            'phone' => 'nullable|string|max:255',
            'mobile' => 'nullable|string|max:255',
            'birth_date' => 'nullable|date_multi_format:"d/m/Y","j/m/Y","d/n/Y","j/n/Y"',
            'fathers_name' => 'nullable|string|max:255',
            'profession' => 'nullable|string|max:255',
            'identity_card' => 'nullable|string|max:255',
            'subscription' => 'date_multi_format:"d/m/Y","j/m/Y","d/n/Y","j/n/Y"',
            'update' => 'regex:/\d{1,2}\/\d{1,2}\/\d{4}/',
            'image' => 'nullable|mimes:gif,jpg,jpeg,png,webp|max:2048',
            'image_delete' => 'boolean',
        ];
    }
}
