<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Database\Eloquent\Relations\Relation;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'club' => 'App\Models\Club',
            'person' => 'App\Models\Person',
        ]);

        Paginator::useBootstrap();

        RateLimiter::for('emails', function ($job) {
            return Limit::perMinute(1)->by($job->user->id);
        });

        Validator::extend('date_multi_format', function($attribute, $value, $formats) {
            // iterate through all formats
            foreach($formats as $format) {

                // parse date with current format
                $parsed = date_parse_from_format($format, $value);

                // if value matches given format return true=validation succeeded
                if ($parsed['error_count'] === 0 && $parsed['warning_count'] === 0) {
                return true;
                }
            }

            // value did not match any of the provided formats, so return false=validation failed
            return false;
        });
    }
}
