<?php

namespace App\Models;

use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class Club extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'active' => 'boolean',
        'normal_member' => 'boolean',
        'in_athens' => 'boolean',
        'in_major_city' => 'boolean',
        'gga_recognized' => 'boolean',
        'founded' => 'date:d/m/Y',
        'subscription_date' => 'date:d/m/Y',
        'membership_date' => 'date:d/m/Y',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function (Club $club) {
            $club->slug = self::get_unique_slug($club);
        });

        self::updating(function (Club $club) {
            $club->slug = self::get_unique_slug($club);
        });
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function manager()
    {
        return $this->belongsTo(Person::class);
    }

    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function athletes()
    {
        return $this->hasMany(Athlete::class);
    }

    public function abilities()
    {
        return $this->belongsToMany(
            Ability::class,
            'ability_club_person',
        )->withPivot('id', 'active', 'starting_date', 'ending_date');
    }

    public function setFoundedAttribute(?string $value)
    {
        $this->attributes['founded'] = getDBDateFromInput($value);
    }

    public function setSubscriptionDateAttribute(?string $value)
    {
        $this->attributes['subscription_date'] = getDBDateFromInput($value);
    }

    public function setMembershipDateAttribute(?string $value)
    {
        $this->attributes['membership_date'] = getDBDateFromInput($value);
    }

    public function headquarters()
    {
        return $this->addresses
            ->where('main', true)
            ->first();
    }

    public function nonHeadquarters()
    {
        return $this->addresses->filter(function ($item) {
            return $item->main == false;
        });
    }

    private static function get_unique_slug(Club $club): string
    {
        $slug = (string) Str::of($club->name)->slug();
        $existing = Club::where('id', '<>', $club->id)
            ->select('slug')->get()->pluck('slug');

        $counter = 1;

        while ($slug === '' || $existing->search($slug) !== false) {
            $slug = 'none-' . $counter;
            $counter++;
        }

        return $slug;
    }
}
