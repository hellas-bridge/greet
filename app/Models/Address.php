<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    public $timestamps = false;

    public function addressable()
    {
        return $this->morphTo();
    }

    public function getPhoneAttribute(?string $value): ?string
    {
        return $this->format_phone_number($value);
    }

    public function setPhoneAttribute(?string $value)
    {
        $this->attributes['phone'] = $this->strip_spaces($value);
    }

    public function toMultiline()
    {
        return $this->street . "\r\n"
            . $this->zip . ' '
            . $this->region . ' '
            . $this->city . "\r\n"
            . ($this->country ? $this->country . "\r\n" : '')
            . "<a href=\"tel:{$this->phone}\">{$this->phone}</a>";
    }

    private function strip_spaces(?string $value): ?string
    {
        if ($value) {
            $value = preg_replace("/[^\d]/", "", $value);
        }
        return $value;
    }

    private function format_phone_number(?string $value): ?string
    {
        if ($value && strlen($this->strip_spaces($value)) == 10) {
            $value = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1 $2 $3", $this->strip_spaces($value));
        }
        return $value;
    }
}
