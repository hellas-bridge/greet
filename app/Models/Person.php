<?php

namespace App\Models;

use App\Models\Club;
use App\Traits\SelfReferenceTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Person extends Model
{
    use HasFactory, SelfReferenceTrait, SoftDeletes;

    protected $guarded = ['id'];

    protected $attributes = [
        'male' => true,
    ];

    protected $casts = [
        'birth_date' => 'date:d/m/Y',
    ];

    public function asset()
    {
        return $this->hasOne(Club::class, 'manager_id');
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function athlete()
    {
        return $this->hasOne(Athlete::class);
    }

    public function abilities()
    {
        return $this->belongsToMany(
            Ability::class,
            'ability_club_person',
        )->withPivot('id', 'active', 'starting_date', 'ending_date');
    }

    public function getFullNameAttribute(): string
    {
        return "{$this->last_name} {$this->first_name}";
    }

    public function getImageURLAttribute(): string
    {
        if (is_null($this->id)) {
            return '';
        }

        $filename = 'person/' . image_directory_chunk($this->id) . '/' . $this->id . '.webp';
        if (Storage::exists('public/' . $filename)) {
            return Storage::url('public/' . $filename);
        }

        if ($this->male) {
            return asset('images/male.svg');
        }

        return asset('images/female.svg');
    }

    public function getImageFilenameAttribute(): string
    {
        if (is_null($this->id)) {
            return '';
        }

        return 'public/person/' . image_directory_chunk($this->id) . '/' . $this->id . '.webp';
    }

    public function getAgeAttribute(): string
    {
        return $this->birth_date ? $this->birth_date->age : '';
    }

    public function getPhoneAttribute(?string $value): ?string
    {
        return $this->format_phone_number($value);
    }

    public function setPhoneAttribute(?string $value)
    {
        $this->attributes['phone'] = $this->strip_spaces($value);
    }

    public function getMobileAttribute(?string $value): ?string
    {
        return $this->format_phone_number($value);
    }

    public function setMobileAttribute(?string $value)
    {
        $this->attributes['mobile'] = $this->strip_spaces($value);
    }

    public function setBirthDateAttribute(?string $value)
    {
        $this->attributes['birth_date'] = getDBDateFromInput($value);
    }

    private function strip_spaces(?string $value): ?string
    {
        if ($value) {
            $value = preg_replace("/[^\d]/", "", $value);
        }
        return $value;
    }

    private function format_phone_number(?string $value): ?string
    {
        if ($value && strlen($value) == 10) {
            $value = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1 $2 $3", $value);
        }
        return $value;
    }
}
