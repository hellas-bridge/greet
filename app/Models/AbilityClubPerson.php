<?php

namespace App\Models;

use App\Traits\CanGetTableNameStatically;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AbilityClubPerson extends Pivot
{
    use CanGetTableNameStatically;

    public $incrementing = true;
    public $timestamps = false;

    // protected $guarded = ['id'];
    protected $table = 'ability_club_person';

    protected $casts = [
        'starting_date' => 'date:d/m/Y',
        'ending_date' => 'date:d/m/Y',
        'active' => 'boolean',
    ];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function setStartingDateAttribute(?string $value)
    {
        $this->attributes['starting_date'] = getDBDateFromInput($value);
    }

    public function setEndingDateAttribute(?string $value)
    {
        $this->attributes['ending_date'] = getDBDateFromInput($value);
    }
}
