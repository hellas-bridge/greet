<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Athlete extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'subscription' => 'date:d/m/Y',
        'update' => 'date:d/m/Y',
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function setSubscriptionAttribute(?string $value)
    {
        $this->attributes['subscription'] = getDBDateFromInput($value);
    }

    public function setUpdateAttribute(?string $value)
    {
        $this->attributes['update'] = getDBDateFromInput($value);
    }

    public function getUpdateColorAttribute(): string
    {
        if (is_null($this->update)) {
            return '';
        }
        $now = date('Y');
        $update = $this->update->format('Y');

        $color = '';
        if ($now <= $update) {
            $color = 'green';
        } elseif ($now == $update + 1) {
            $color = date('m') <= 5 ? 'orange' : 'red';
        } elseif ($now == $update + 2) {
            $color = 'red';
        } else {
            $color = 'black';
        }

        return "<span class=\"color-code {$color}\"></span>";
    }
}
