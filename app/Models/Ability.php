<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ability extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $guarded = ['id'];

    public function people()
    {
        return $this->belongsToMany(
            Person::class,
            'ability_club_person',
        )->withPivot('id', 'active', 'starting_date', 'ending_date');
    }

    public function clubs()
    {
        return $this->belongsToMany(
            Club::class,
            'ability_club_person',
        )->withPivot('id', 'active', 'starting_date', 'ending_date');
    }
}
